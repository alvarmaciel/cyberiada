+++
title = "Medidor de Amor"
lastmod = 2019-10-11T15:56:55-03:00
tags = ["microbit"]
categories = ["nerdeadas"]
draft = false
image = "https://user-content.gitlab-static.net/034b9c13e709078cc00c2a49ccc9d23b20c7dd02/68747470733a2f2f7078742e617a757265656467652e6e65742f626c6f622f633530363334393633633666643939653532376539393538323937396334626131623138376566312f7374617469632f6d622f70726f6a656374732f6c6f76652d6d657465722f6c6f76652d6d657465722e676966"
subtitle = "Proyecto sencillo para empezar con Micro:Bit"
+++

Vamos a hacer una medidor de amor ¡Qué ternura!.

Esta es una traducción del tutorial [LOVE METTER](https://makecode.microbit.org/) de MakeCode.

Un jueguito para que la Micro:Bit nos de un número al azar cuando tocamos el PIN 0

<!--more-->

Para construir nuestra máquina medidora del amor:

1.  Agerguen un bloque "Al presionar PIN 0" para que nuestro programa arranque cuando se presiona el pin 0. Pongan P0 en la lista de pines que pueden elegir en este bloque.
2.  Usando los bloques "mostrar número", que está en básicos y "escoger al azar de" de la paleta matemática armen un bloque que muestre un número al azar entre 0 y 100 cuando se presiona el pin 0
    -   [Si están trabados hagan click acá](/images/ayuda1.png)
3.  Hagan click en el pin 0 del simulador para ver que números es el elegido
4.  Mostrar "Medidor de Amor" en la pantalla cuando se incia la Micro:Bit
5.  Hagan click en Descargar para transeferir su código  a la Micro:Bit. Una vez que lo transiferan, al presionar el pin GND con una mano y el PIN0 con la otra mano se activará el código.
