+++
title = "Primeros pasos con Microbit"
lastmod = 2019-04-19T08:18:40-03:00
tags = ["microbit"]
categories = ["nerdeadas"]
draft = false
image = "images/microbit.gif"
+++

Como parte del equipo de Docentes Remotos de pensamiento computacional de la [Fundación Sadosky](http://www.fundacionsadosky.org.ar) para el [Plan Ceibal](https://www.ceibal.edu.uy/es). Además de trabajar con Scratch, empezamos a trabajar con [Micro:bit](https://microbit.org/es/) Una sencilla placa programable que nos permite hacer robótica y todo ese montón de cosas que las placas programables nos permiten hacer.

<!--more-->

Algunas cositas destacadas:

-   Se puede programar por bloques
-   Se puede programar en Python
-   Tiene un array de leds al frente de 5 x 5 que nos permite empezar a progrmarlos de entrada, sin necesitar nada más que el editor para ver un resultado bonito
-   Dos botones de interacción
-   Brujula y gisroscopio
-   Bluethoot
-   Un pack de pilas recargables

Lo importante para mi vida serrana es poder usarla lo más offline posible, así que [Mu-editor](https://codewith.mu/en/) es la opción para linux (makecode queda descartado)

Claro que todavía no está empaquetado para Debian Stretch

{{< figure src="https://media.giphy.com/media/SuJoKxDplnKve/giphy.gif" >}}
