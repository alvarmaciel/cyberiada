+++
title = "Usando Desmos para crear contenido matemático"
lastmod = 2020-03-27T07:26:53-03:00
tags = ["desmos", "matematica"]
categories = ["educación"]
draft = false
image = "https://media.giphy.com/media/J4hIVVNKc0rqdYskmq/giphy.gif"
subtitle = "Plataforma gratuita, más no libre, de generación de contenido matemático"
+++

[Desmos](https://desmos.com) comenzó como una calculadora gráfica de funciones y poco a poco fue creciendo hasta derivar en una plataforma multifuncional de generación de contenidos. La particularidad de este servicio es que cuenta con una serie de tipos de actividades que funcionan como un lienzo en blanco a partir de la cual se pueden realizar todo tipo de secuencias.
Comencé a usar esta plataforma hace dos años, a partir de la necesidad de preparar contenido para una estudiante cuadriplégica y luego se convirtió en material de todo el curso.

[La sección para docentes](https://teacher.desmos.com) de Desmos nos permite crar contenido y monitorear en tiempo real las respuestas de los estudiantes dandonos lugar a hacer puestas en común muy enriquecedoras. Por otro lado, no es necesario que les chiques tengan una cuenta en el sistema para usar las actividades.

{{< figure src="/ingresoDesmos.gif" caption="Figure 1: Ingreso sin registro" >}}

La verdad es que es una plataforma muy completa y compleja. Voy a dejar unos ejemplos de tipos de actividades que armé o encontré en la plataforma.
Si este es un tema que les interesa o quieren profundizar, escriban en los comentarios.


## Algunos ejemplos de uso {#algunos-ejemplos-de-uso}

Esta es una secuencia para quinto grado sobre multiplicación.

{{< figure src="/ox-hugo/numerosMayores.gif" caption="Figure 2: Multiplicar por números mayores" >}}

Acá pueden ver como vemos las respuestas, estas respuestas se visualizan en tiempo real.

{{< figure src="/ox-hugo/dashboardDesmos.gif" caption="Figure 3: Dashbord de una secuencia de geometría. Los nombres de los estudiantes están anonimizados." >}}
