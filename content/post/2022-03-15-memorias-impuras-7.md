+++
title = "Memorias Impuras #7"
lastmod = 2022-03-15T22:16:07-03:00
tags = ["python", "tdd"]
categories = ["memoriasImpuras", "nerdeadas"]
draft = false
image = "https://alvarmaciel.gitlab.io/cyberiada/images/beastars.gif"
+++

Como inyectar un escenario en un test usando fixtures.
Una observación, un llamado y un equipo siempre generoso.

#HermesTeam [@weareshiphero](https://twitter.com/weareshiphero)

IMPORTANTE UPDATE: Los escenarios no son algo definido por Pytest, sino una implementación propia de ShipHero
<!--more-->



Los fixtures son una serie de recursos que se setean antes de que comiencen los test y se &ldquo;limpian&rdquo; después de que el test terminó. Teniendo en cuenta esto

-   Las funciones de tipo Fixtures aprovechan el concepto de &ldquo;inyección de dependencia&rdquo; de la programación orientada a objetos, en el cual al función toma el rol de **injector** y la función del test es considerado un consumidor de los objetos del fixture.

y que pueden definirse alcances de los fixtures (scopes) se convierten en una herramienta muy práctica para el testeo. -> [Más info](https://www.lambdatest.com/blog/test-automation-using-pytest-and-selenium-webdriver/)

Si a esto se le suma la capacidad de definir escenarios, tenemos un compendio de herramientas muy poderosas para testear.

Un escenario es la definición del contenido y las relaciones que puede haber en la base de datos. El framework (pytest en este caso) alimenta la base de datos con estos escenarios que luego pueden ser consumidos por los distintos test.

{{< figure src="/images/beastars.gif" >}}

Este es un ejemplo que:

1.  NO ESTA PROBADO Y PROBABLEMENTE NO ANDE; ES UNA IDEA CONCEPTUAL
2.  LEA 👆🏾
3.  SI TODO MARCHA BIEN EN BREVE SALE POST CON EL TEST BIEN HECHO
4.  Este escenario representa un CRUD de socios de una mutual con relaciones entre el socie y una tienda


```pyhon 
import pytest

SCENARIO = """
    tiendas:
      - id: 1
        ubicacion: San Javier
        nombre: La de Sanja
     - id: 2
      ubicacion: Las Rosas
      nombre: La de las rosas
    socies:
    - nombre: fulano
      apeliido: de tal
      tienda: $tienda_1.id
    - nombre: fulana
      apellido: no de tal
      tienda: $tienda_2.id

""""

@pytest.fixture
def scenario(make_scenario):
    return make_scenario(SCENARIO)


def test_fulano_belong_to_san_javier(scenario):
    socie_1 = scenario.socies[0]
    ubicacion_esperada = scenario.tiendas[0]

    assert socie_1.tienda == ubicacion_esperada.id

```

En este caso defino un escenario, un fixture e inyecto el escenario en el test 🚀



# Notas

-   Nótese que aquello que parece una variable es una referencia interna `$tienda_1.id` es una referencia a la tabla `tiendas`, primer registro, campo `id`. En este caso la de `nombre: San Javier`
-   El escenario se escribe en formato YAML



