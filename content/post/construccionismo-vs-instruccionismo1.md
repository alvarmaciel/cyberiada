+++
title = "Construccionismo Vs. Instruccionismo 1"
lastmod = 2019-09-14T14:54:20-03:00
tags = ["educacion"]
categories = ["traducciones"]
draft = false
image = "https://media.giphy.com/media/fjgqYjVkzfQ9a/giphy.gif"
+++

Traducción apocrifa de la primera parte de una conferencia de Seymour Papert. La traduje en lenguaje inlcusivo. Ofendides... dejen comentarios en los [Isuues del proyecto](https://gitlab.com/alvarmaciel/traducciones/issues) o si querés participar de la traducción de las partes que faltan, [lo estoy haciendo acá](https://www.transifex.com/amaciel/articulos-de-papert)

Original: [Construccionismo vs Instruccionismo](http://www.papert.org/articles/const%5Finst/const%5Finst1.html)

<!--more-->

En los años de 1980 Seymour Papert dió la siguiente charla por video para una conferencia de educadores en Japón


## Parte 1: Enseñanza vs. Aprendizaje {#parte-1-enseñanza-vs-dot-aprendizaje}

[Archivo de video](http://www.papert.org/media/video/articles/const%5Finst/const%5Finst1.ram)

Seymour Papert: Hola. Saludos a les educadores Japoneses. Lamento no estar ahí con ustedes, pero afortunadamente vivimos en un siglo de tecnología, y puedo usar esta máquina magica creadora-de-imágenes para enviarles mi voz e imagen a través de la mitad del mundo. Es realmente asombroso.

De lo que iba a hablar, si estuviera ahí, es sobre cómo la tecnología puede cambiar la forma en que les niñes aprenden matemáticas. Y digo cómo les niñes pueden aprender matemáticas de forma diferente, no así, cómo nosotres podemos enseñar matemáticas de forma diferente. Esta es una importante distinción.

Todo mi trabajo está centrado en colaborar en el aprendizaje de les niñes, no solo en la enseñanza. Acuñé una frase para esto: Construccionismo e Instruccionismo son nombres para dos abordajes en la innovación educativa. Instruccionismo es la teoría que dice, "Para tener mejor educación, debemos mejorar la instrucción. Y si vamos a usar computadoras, haremos que las computadoras se encarguen de la instrucción." Y eso nos lleva a toda la idea de enseñanza asistida por computadora.

Bueno, enseñar es importante, pero aprender es mucho más importante. Y el construccionismo significa: "Darle a les niñes buenas cosas para hacer, así aprenden haciendo, de mucha mejor manera de la que lo hacían antes." Ahora bien, Creo que las nuevas tecnologías son muy, muy ricas a la hora de proveer cosas para hacer a los chicos de forma que puedan aprender matemáticas como parte de algo real.

Creo que parte del problema con el aprendizaje de las matemáticas en la escuela es que no son como las matemáticas del mundo real, hay ingenieres que usan las matemáticas para hacer puentes o hacer máquinas. Hay científique que usan las matemáticas para hacer teorías, para crear explicaciones sobre como funcionan los átomos , y como empezó el universo. Les banqueres, que usan las matemáticas para hacer dinero -- o eso creen elles.

Pero en el caso de les niñes ¿Qué es lo que pueden hacer con las matemáticas? No mucho. Elles se sientan en las clases y escriben números en papeles. Eso no implica hacer nada muy emocionante. Así que tratamos de encontrar formas en la que les niñes puedan usar las matemáticas para hacer algo --cosas interesantes, de manera que la relación de les niñes con las matemáticas sea más parecida a la de les ingenieres, o les científiques, o les banqueres, o la de toda esa gente importante que usa las matemáticas constructivamente para hacer algo.

Entonces ¿Qué tipo de cosas se pueden hacer? Déjenme darles algunos ejemplos: una niña de una escuela de California, la Gardener Academy, en un proyecto llamado MindStorms, hizo un calendario. Y este es un trabajo de un cuarto grado, donde programó la computadora para hacer la estructura, pensando en como combinar cuadrados y triángulos para que encajen juntos. Y cómo tenían que hacer todo esto a una computadora, escribiendo programas en LOGO, ella usó matemáticas reales para hacer algo que le gustaba, que tuvo incluso valor comercial, porque vendieron estos calendarios para conseguir dinero para mejorar sus proyectos.

Algunas de las cosas que hicieron son simplemente bellas. Por ejemplo esta que me llegó justamente hoy y de la que estoy tan contento. Me lo envió una niña de Costa Rica, de lo profundo de America Central. la niña programó la computadora en LOGO, usando LOGOWriter e hizo esta belleza. La fotografiaron y me la enviaron.

Ahora si le preguntan a esta niñas, mientras hacía el dibujo ¿Qué estás haciendo? no creo que ella responda "estoy programando una computadora" o que ella diga "estoy haciendo matemáticas". Ella seguro diría, "Estoy haciendo un pájaro, estoy haciendo un dibujo, lo voy a mandar a NorteAmérica" Ella hubiera expresado la emoción de lo que estaba haciendo.

Pero si miramos con atención lo que ella hizo, verán que tuvo que ocuparse de la descripción matemática de una curva como gráfico. Se tuvo que ocupar de las descripciones matemáticas de las formas. Ella estuvo haciendo matemáticas, como una persona real -- una matemática real, una ingeniera, una científica.

Y esto es lo que intentamos hacer: encontrar formas en las cuales la tecnología permita a les niñes usar el conocimiento,conocimiento matemático y otros tipos de conocimientos, no solo acumularlos en la cabeza de forma que doce años más tarde sea utiles. Nadie puede aprender bien de esa manera, es una terrible forma de aprendizaje. Todes queremos aprender de manera que podamos usar eso que aprendimos, y eso es lo que intentamos hacer con estes niñes.
