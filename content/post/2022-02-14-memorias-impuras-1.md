+++
title = "Memorias Impuras #1"
lastmod = 2022-02-14T22:17:13-03:00
tags = ["python", "Domain Drive Design"]
categories = ["nerdeadas", "memoriasImpuras"]
draft = false
image = "https://c.tenor.com/VLLJuwYmqkkAAAAM/error-wait.gif"
+++

Hace un tiempo que al terminar mi jornada, me tomo unos minutos para escribir en un cuaderno qué es lo que aprendí. De esta forma, me doy una especie de palmadita en el hombro por la jornada y trato de recapitular el día.
Obvio, no aprendo todos los días algo. Pero todos los días escribo algo.
Quiero dar un paso más y tratar de registrar fuentes, ideas y cosas que aprendo de forma asistemática, enredada y solo hilada por un hashtag en redes y este registro bloguero.

Entonces ¿Qué aprendí hoy?
- Que se puede planear o pensar una aplicación donde el ORM dependa de modelo y no a la inversa. Al parecer [SQLAlchemy](https://docs.sqlalchemy.org/en/14/orm/mapping_styles.html#classical-mappings) lo hace sencillo. Repository Pattern de [cosmicPython](https://www.cosmicpython.com/book/chapter_02_repository.html#_inverting_the_dependency_orm_depends_on_model)
- El módulo [logging](https://docs.python.org/es/3/howto/logging.html) me permite manejar los logs en python. Tiene unas cuantas cosas interesantes, como definir customs logger a partir de archivos yml. Lástima que solo saca la info en string. Pero existe [struclog](https://www.structlog.org/en/stable/) que lo puede ¿parsear? a leibles JSON. Estoy verde en esto pero vamos pa' delante. ya me hice el cursito [Logging in Python](https://realpython.com/courses/logging-python/) y vamos por más.
