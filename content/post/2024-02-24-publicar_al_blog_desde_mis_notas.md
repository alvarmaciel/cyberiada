+++
title = "Publicar al blog desde mis notas"
date = 2024-02-24T10:05:00-03:00
lastmod = 2024-02-24T11:11:16-03:00
tags = ["emacs", "org-roam", "hugo"]
categories = ["nerdeadas"]
draft = false
image = "https://media.giphy.com/media/l2QZUJ0IubBLIf8A0/source.gif"
subtitle = "Vuelta al ruedo"
+++

Si siguen este blog, sabran que soy un usuario de [emacs](https://www.gnu.org/software/emacs/) y de [org-mode](https://orgmode.org/). Un viejo y desprolijo usuario que publica poco. Este es un paso para cambiar esto último.

<!--more-->

Ya hace unos años me embarqué en la toma de notas y apuntes usando [org-roam](https://www.orgroam.com/).
Ahora, que quiero volver a publicar en mi blog. Quisera poder usar todo lo que me ofrece org-roam para escribir.

Este es el primer paso. Escribir desde roam usando sus capacidades de linkeo
Ojalá esto ande, es solo una prueba. Lo único que hice fue agregar este bloque a mis configuraciones

```lisp
(setq org-roam-capture-templates
      '(("d" "default" plain
         "%?"
         :if-new (file+head "%<%Y-%m-%d>-${slug}.org"
                            "#+title: ${title}\n")
         :unnarrowed t)
        ("c" "Cyberiada article" plain
         "%?"
         :if-new
         (file+head "articles/cyberiada/%<%Y-%m-%d>-${slug}.org"
                    "#+HUGO_BASE_DIR: ~/git/cyberiada
                    #+HUGO_SECTION: ./post
                    #+HUGO_FRONT_MATTER_KEY_REPLACE: author>nil
                    #+HUGO_AUTO_SET_LASTMOD: t
                    #+TITLE: ${title}
                    #+DATE: %U
                    #+HUGO_TAGS: tags
                    #+HUGO_CATEGORIES: categories
                    #+HUGO_DRAFT: true
                    #+HUGO_CUSTOM_FRONT_MATTER: image \"link to image\" :subtitle \"some subtitle\"\n")
         :immediate-finish t
         :unnarrowed t)))
```

Esta es un imagen de mi nota a la derecha, cuando investigaba y como quedó el archivo final en org antes de exportarlo a [Hugo](https://gohugo.io/)

{{< figure src="/Captura desde 2024-02-24 10-55-09.png" >}}
