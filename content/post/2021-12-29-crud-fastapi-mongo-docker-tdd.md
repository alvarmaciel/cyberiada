+++
title = "Cómo hacer un CRUD con FastAPI + MongoDB"
lastmod = 2021-12-29
tags = ["python", "fastapi", "mongodb", "docker", "tdd"]
categories = ["nerdeadas"]
draft = true
subtitle = "Paso a paso de la construcción de una API usando Docker y testeando en cada paso"
+++

En este tutorial vamos a seguir los pasos para crear una API asincrónica con FastAPI y MongoDB. Escribo estas guías para aprender, lo que hago es tomar otros tutoriales o videos, en general en inglés. Seguirlos, traducirlos y luego incorporarlos a un proyecto propio.
Este tutorial está hecho en base a [Building a CRUD App with FastAPI and MongoDB](https://testdriven.io/blog/fastapi-mongo/#mongodb) de [Abdulazeez Abdulazeez Adeshina Abdulazeez Abdulazeez Adeshina](https://testdriven.io/authors/adeshina/), [docker-nginx-django-vue-architecture](https://gitlab.com/briancaffey/docker-nginx-django-vue-architecture/-/blob/master/documentation/README.md) y [Test Driven Developmnent of a Django RESTfull API](https://realpython.com/test-driven-development-of-a-django-restful-api/)
<!--more-->

## Por qué usar contenedores
Hace no mucho empecé a utilizar contenedores para el desarrollo de algunas aplicaciones. Mi objetivo principal es aislar el sistema operativo de mi computadora personal del entorno de desarrollo, aprender en el proceso y dejar los proyectos listos para que otra persona pueda bajarlo y correrlo en sus máquinas.
Dado que en mi notebook uso [Fedora](https://start.fedoraproject.org/), cuento con una herramienta muy buena para gestionar contenedores: [Toolbox](https://docs.fedoraproject.org/en-US/fedora-silverblue/toolbox/). Y si bien el presente proyecto lo hice usando Toolbox. Voy a escribir este artículo usando [Docker](https://www.docker.com) y [docker-compose](https://docs.docker.com/compose/).

## Dominio
La Mutual Personas nos pidió hacer un sistema....

## Configurando el entorno
El desarrollo lo llevaré a cabo en la carpeta *api-socies* esta carpeta será la raiz de un repositorio git en el cual iremos desarrollando toda la API
<INSERTAR DESPLIEGUE DEL TREE>>
### Git setup
Crearemos un nuevo directorio, este directorio contendrá el repositorio local que mantendremos en GitLab. 

```bash
mkdir api-socies
cd api-socies
```

### Docker
Dado que utilizo GNU/Linux, las instrucciones y links serán para este Sistema Operativo.

Seguir las instrucciones para instalar la community edition de docker. Pueden encontrarlas [acá](https://docs.docker.com/engine/install/)

Después de tener instalado docker sigan los [Pasos post-instalacion para linux](https://docs.docker.com/install/linux/linux-postinstall/). Estos pasos permiten usar docker sin tener que anteponer `sudo` en cada comando.

Para finalizar, instalar =docker-compose=. [[https://docs.docker.com/compose/install/#install-compose][Siguiendo estas instrucciones]]

Asegúrense de que docker está correctamente configurado en sus máquinas corriendo el siguiente comando:

```bash
docker run hello-world

Hello from Docker!
This message shows that your installation appears to be working correctly.

To generate this message, Docker took the following steps:
 1. The Docker client contacted the Docker daemon.
 2. The Docker daemon pulled the "hello-world" image from the Docker Hub.
    (amd64)
 3. The Docker daemon created a new container from that image which runs the
    executable that produces the output you are currently reading.
 4. The Docker daemon streamed that output to the Docker client, which sent it
    to your terminal.

To try something more ambitious, you can run an Ubuntu container with:
 $ docker run -it ubuntu bash

Share images, automate workflows, and more with a free Docker ID:
 https://hub.docker.com/

For more examples and ideas, visit:
 https://docs.docker.com/get-started/
```

Si usaron docker previamente, tal vez quieran remover las imágenes viejas o sin usar. Esto lo pueden hacer con los siguientes comandos

```bash
docker system prune
docker rmi $(docker images -f "dangling=true" -q)
docker rmi $(docker images -a -q)
docker rm $(docker ps --filter=status=exited --filter=status=created -q)
```

### GIT

Nuestro código vivirá de forma local en nuestra máquina y de forma remota en [GitLab](https://gitlab.com) pero a los fines prácticos de este tutorial, por ahora no vamos a conectar el local con el remoto.

Inicializaremos el repositorio y crearemos un archivo README.md
```bash
git init
echo "## API de Mutual PERSONAS" >> README.md
```

Luego agregamos el archivo recién creado al stage para confirmar los cambios en el repositorio, de esta manera, ya con un `commit` realizado podemos empezar a armar las ramas de nuestro repo.

```bash
$ git add README.md
$ git commit -m "Comit Incial"
[main (commit-raíz) 566da56] Comit Incial
 1 file changed, 1 insertion(+)
 create mode 100644 README.md
```


Este proyecto intenta adherirse a las prácticas descriptas [acá](https://nvie.com/posts/a-successful-git-branching-model/)
Empezaremos creando una rama `develop`, luego en base a esa rama creamos la rama en la que vamos a realizar casi todo el trabajo, donde agregaremos la aplicación que construiremos con FastAPI dockerizada

```bash
git branch develop
git checkout -b fastapiapp develop
git branch
  develop
* fastapiapp
  main

```
<!---
### ENV
Una buena práctica para poder compartir código es tener ambientes separados en cada proyecto. Para eso, vamos a:
1. crear un ambiente Python en la raíz de nuestro proyecto
2. Incorporar los requerimientos de nuestro proyecto en un archivo *requirements.tx*
3. Decirle a git que ignore una serie de archivos y carpetas para que no los suba la repositorio remoto

#### Crear y activar el ambiente
Asegurase de tener instalado Python y Python venv en sus máquinas y ejecuten `python -m venv venv` esto les creará un directorio /venv/ donde se guardaran todoas las librerías que usaremos en el proyecto.

Para ingresar al ambiente recién cread, desde su directorio raíz tienen que ejecutar `source venv/bin/activate` y dependiendo de sus configuración verán en el prompt de su terminal alguna indicación de que están en el ambiente que crearon. El mío me avisa así

{{<figure src="/images/prompt_ambiente.png">}}

-->


### DOCKER

Vamos a levantar dos contenedores para nuestra app, en uno vivirá la aplicación y en otro la base de datos, para eso usamos **docker-compose**
en nuestro directorio raíz agregamos el siguiente archivo *docker-compose.yml*:

```
version: '3.8'

services:
  api:
    build: ./app
    volumes:
      - .:/code
    ports:
      - '8000:8000'
    environment:
      - DB_URL=mongodb://db/myTestDB
    depends_on:
      - db
  db:
    image: mongo
    ports:
      - 27017:27017
    volumes:
      - taskdb:/data/db


volumes:
    taskdb:

```

Este archivo indica a Docker como realizar algunas de las tareas que necesitamos veamos este archivo por partes:

- Dentro de `services` declaramos los servicios que vamos a levantar:
  - `api`: nombre del servicio
    - `buid`: lugar donde buscar el archivo *Dockerfile+ para construir el contenedor
    - `volumes`: Indicamos que construya el contenedor con los datos del directorio donde estara *Dockerfile* (todavía no lo creamos) y que guarde esos datos en el contendero dentro de la carpeta //code/
    - `ports`: Con este comando indicamos que puentee el puerto 8000 de nuestro contenedor al puerto 8000 de nuestra máquina (host)
    - `environment`: define una variable para conectarse con la base de datos
    - `depends_on`: indica que necesita que está corriendo el contenedor de la base de datos para correr la app
  - `db`: nombre del servicio de la base de datos
    - `image`: la imagen que vamos a usar, en este caso no necesitamos meternos en los detalles de la configuración de la base, lo haremos todo desde la app, por lo cual usamos una imagen
    - `ports`: Igual que antes, conectamos los ports del contenedor con el host
    - `volumes`: Indicamos unos volumenes para poder tener persistencia de los datos de la Base de Datos


Ahora vamos a empezar a crear nuestra app propiamente dicha. Para eso primero creamos un directorio donde agregaremos los archivos necesarios para iniciarla la API, iniciar el contenedor y los requerimientos 


```
mkdir app
cd app
```

Dentro de /app/ crearemos el archivo *Dockerfile* con los elementos para que levante nuestro contenedor 

```
# Pull de la imagen base oficial   
FROM python:3.9.6-alpine
                                           
                                           
# setup del directorio de trabajo   
WORKDIR /code              
                                           
# Configuración de las variables de ambiente
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1



RUN pip install --upgrade pip
COPY requirements.txt /code/
RUN pip install -r requirements.txt


COPY . ./code/

```
Agregamos los requerimientos de nuestro proyecto, para eso creamos el archivo */app/requirements.txt/* con el siguente contenido:

```
fastapi == 0.70.1
uvicorn == 0.16.0
pydantic[email]
motor == 2.5.1
```

Ahora solo nos queda crear los archivos que iniciarán la aplicación. Dentro del directorio *app* crear un archivo llamado *__init__.py* vacio y uno *main.py*
**__init__.py**
```bash
touch __init__.py
```

**main.py**
```python
import uvicorn

if __name__ == "__main__":
    uvicorn.run("server.app:app", host="0.0.0.0", port=8000, reload=True)
```


Luego veremos para que es cada línea. Basta con saber que lo que hará es instalar en el ambiente que crea Docker las librerías FastAPI, Uvicorn, de Pydantic el módulo Email y la librería Motor


Nuestro prooyecto por ahora se estructura de la siguiente forma
```
.api-socies # Directorio raíz inciado con git
├── app # Directorio de nuestra aplicación
│   ├── Dockerfile # Reglas para construir el contenedor de la API
│   └── requirements.txt # Requerimientos necesarios para que corra la app
├── docker-compose.yml # Reglas para levantar los contenedores de la api y la base de datos
└── README.md # Archivo con detalles del proyecto (Solo un título por ahora)


```

ahora agregaremos un =Dockerfile=, =docker-compose.yml= y =requeriments.txt=:

*Dockerfile*
#+begin_src
# Pull de la imagen base oficial
FROM python:3.9.6-alpine

# setup del directorio de trabajo
WORKDIR /usr/src/app

# Configuración de las variables de ambiente
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

# instalar psycopg2 dependencias
RUN apk update \
    && apk add postgresql-dev gcc python3-dev musl-dev


# instalacion de dependencies
RUN pip install --upgrade pip
COPY ./requeriments.txt .
RUN pip install -r requeriments.txt

# copiar proyecto
COPY . .
#+end_src

*requeriments.txt*

#+begin_src
Django
psycopg2-binary
#+end_src

*docker-compose.yml*

#+begin_src
version: '3.8'

services:
  web:
    build: .
    command: python manage.py runserver 0.0.0.0:8000
    volumes:
      - .:/usr/src/app
    ports:
      - '8000:8000'
    depends_on:
      - db
  db:
    image: postgres
#+end_src
 Por ahora la estructura de nuestro directorio raíz es:

