+++
title = "Cazadoras de hongos"
lastmod = 2017-12-03
tags = ["poesía"]
categories = ["traducciones"]
draft = false
image = "/images/mushroom.gif"
subtitle = "Traducción de un poema, de Neil Gaiman y recitado por Amanda Palmer publicado en Brain Picking"
+++

Traducción del poema sobre la ciencia de Neil Gaiman, recitado por Amanda Palmer y publicado en [Brain Pickings](http://bit.ly/2rNRWZc)

<!--more-->

{{< figure src="/images/mushroom.gif" >}}

```
La ciencia, como bien sabes mi pequeña, es el estudio
de la naturaleza y el funcionamiento del universo.
Está basada en la observación, la experimentación, la medición,
y la formulación de leyes que describan los hechos revelados.

Dicen que en los tiempos remotos,los hombres vinieron preparados con cerebros
diseñados para perseguir  a las bestias de carne a la carrera,
para atravesar ciegamente lo desconocido,
y para luego encontrar el camino a casa si se perdían
cargando con un antilope muerto.
O, en un mal día de caza, nada.

Las mujeres, que no necesitaban correr a las presas,
tenían cerebros que detectaban marcas en el terreno y creaban caminos
entre la izquierda del aquel arbusto espinoso y aquella bajada de piedritas
y miraban hacia abajo, a la sombra de aquel árbol medio caido,
porque algunas veces había hongos.

Antes del garrote de pedernal, o la cuchilla de piedra carnicera.
La primera herramienta de todas fue un saco para colgar al bebe,
para tener nuestras manos libres
y algo en lo que poner las bayas y los hongos,
las raíces y las buenas hierbas, las semillas e insectos.
Luego, un mortero para aplastas, machacar, moler o romper.

A veces los hombres perseguían a las bestias
a lo profundo del bosque,
y nunca máś volvían.

Algunos hongos te matarán,
mientras otros nos mostraran a las diosas
y algunos alimentaran el hambre en nuestras barrigas. Identificar.
Otros nos matarán si los comemos crudos,
y nos matarán de nuevo si los cocinamos una vez,
pero si los hervimos en agua de manantial, y dejamos el agua de lado,
y los volvemos a hervir una vez más, y dejamos el agua de lado,
solo así podremos comerlo seguras. Observar.

Observar los nacimientos, medir el crecimiento del vientre y la forma de los pechos,
y a través de la experiencia descubrir cómo traer sin peligro bebes al mundo.

Observar todo.

Y las cazadoras de hongos caminan como caminan
y miran el mundo, y observan lo que ven.
Y algunas de ellas crecerán y se relameran los labios,
mientras otras se agarraran los estómagos y morirán.
Así las leyes sobre lo que es seguro fueron hechas y heredadas. Formular

Las herramientas que hacemos para construir nuestras vidas:
nuestras ropas, nuestra comida, nuestro camino al hogar...
Todas estas cosas las basamos en la obervación,
la experimentación, la medición, la verdad.

Y la ciencia, recuerda, es el estudio
de la naturaleza y el comportamiento del universo,
basada en la observación, experimentación, y medición
y la formulación de leyes para describir estos hechos.

La carrera continúa. Una joven científica
dibuja bestias sobre las paredes de la cueva
para mostrar a sus pequeñas criaturas, ahora que están gordas de hongos
y bayas, qué es seguro cazar.

Los hombres corren tras las bestias.

Las científicas caminan lento, sobre la cima de la colina
y bajo la orilla y pasan por el lugar donde fluye la arcilla roja.
Ellas cargan sus bebes en los sacos que hicieron,
liberando sus manos para recoger hongos.
```

<p><iframe src="https://player.vimeo.com/video/214686538" align="middle" width="640" height="360" frameborder="0" allowfullscreen=""> </iframe>

<p><a href="https://vimeo.com/214686538">The Universe in Verse finale: Amanda Palmer reads &quot;The Mushroom Hunters&quot; by Neil Gaiman</a> from <a href="https://vimeo.com/brainpicker">Maria Popova</a> on <a href="https://vimeo.com">Vimeo</a>.</p>
