+++
title = "Modulo turtle en Jupyter"
lastmod = 2020-05-07T18:32:41-03:00
tags = ["python"]
categories = ["educación"]
draft = true
image = "https://media.giphy.com/media/J4hIVVNKc0rqdYskmq/giphy.gif"
subtitle = "Habilitar turtle en jupyter para armara tareas de programación para niñes"
+++

```shell
pip install ipyturtle
jupyter nbextension enable --py --sys-prefix ipyturtle
```

```python
from ipyturtle import module
t = turtle()
t
```
