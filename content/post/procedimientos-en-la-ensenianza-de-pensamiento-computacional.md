+++
title = "Procedimientos en la enseñanza de Pensamiento Computacional"
lastmod = 2020-02-07T12:23:07-03:00
categories = ["educación"]
draft = false
image = "https://gitlab.com/alvarmaciel/cyberiada/-/raw/master/content/images/caidaDeNieve.gif"
subtitle = "pensando la enseñanza"
+++

El pensamiento computacional está emergiendo como un área relativamente nueva de enseñanza que enlaza multiples disciplinas y convoca a les profesionales involucrades en áreas tecnológicas de la enseñanza. El trabajo de Jeannette Wing lidera las referencias bibliográficas y sería deshonesto obviar a Seyumour Pappert como agente impulsor de una nuevo marco de referencia en torno al uso de dispositivos digitales en la enseñanza. Lideran el ejercicio teórico que miles de docentes llevamos a las aulas. Es en este  ejercicio desde donde tenemos que plantearnos preguntas para avanzar en un campo nuevo y multidisciplinario.

Trabajando con niños y niñas de escuela primaria, cabe preguntarnos si estamos enseñando a programar o a resolver problemas en diferentes niveles de abstracción. Y esta pregunta interpela nuestros conocimientos técnicos y pedagógicos.

Ante un problema sencillo en una animación, qué sentido está implícito en estos dos programas:

|                                                |                                                |
|------------------------------------------------|------------------------------------------------|
| ![](/ox-hugo/copoDeNieveConProcedimientos.png) | ![](/ox-hugo/copoDeNieveSinProcedimientos.png) |

Ambos algoritmos cumplen con su objetivo, pero uno de ellos modela la solución en varias capas de abstracción. Ese es el poder de la enseñanza de los procedimientos.
Modelar en distintos niveles de abstracción el problema. Ya no se trata de tener un programa ejecutandose correctamente, sino de pensar lo que el programa tiene que hacer.  Abstraer y subdividir, generar nuevas abstracciones que den cuenta de  cada elemento de la solución.
Ahora cabe preguntarnos ¿Y qué enseñamos primero?... Aunque esa es una pregunta para otro artículo.
