+++
title = "Memorias Impuras #6"
lastmod = 2022-03-10T10:16:07-03:00
tags = ["python", "tdd"]
categories = ["memoriasImpuras", "nerdeadas"]
draft = false
image = "https://alvarmaciel.gitlab.io/cyberiada/images/obey_the_testing_goat.png"
+++

Aprender toma tiempo. Este post es una forma de procesar lo que hice, de aprehenderlo, un intento de trascender la memoria.

No sé aún como agradecer al equipo Hermes dentro de [ShipHero](https://shiphero.com). Son lo mejor.

<!--more-->

> `>>>import this`
>
> 3.  Complejo es mejor que complicado
>
>   \- Zen de Python

{{< figure src="/images/obey_the_testing_goat.png" >}}


## Testing {#testing}

"Usa cualquier framework, pero haz tus test". Creo que es un camino de ida. Me asignaron un ticket, lo encaré como siempre, derecho al código. Vi los test que se rompían. Y me di cuenta que en ellos había más que una verificación. Había una forma de entender lo que se había programado.
Deshice mis modificaciones y le dediqué una buena parte de mi tiempo a entender. Y sentía que a medida que el tiempo pasaba, más defraudaba a mi equipo y a mi mismo. Metí mano, volví a romper, volví a deshacer y retomé el capítulo 1 de [Test Driven Development in Python](https://obeythetestinggoat.com) y todo cayó en su sitio:

1.  Lo que había leído no se me había impregnado
2.  Un viejo axioma de mis años docentes se aplica y parafrasea para el desarrollo de sistemas complejos:

> Ninguna planificación resiste el contacto con los estudiantes.
>
> \- De cuando doy clases.
>
> Ninguna planificación resiste el contacto con el código vivo.
>
> \- De cuando programo.

Así que volví para atrás y me puse a pensar como hacer estos test que deberían probar la escritura en una base de datos. Me valí de tests ajenos, rastree, indagué, copie (no tengo miedo a aceptarlo, aunque me limité a copiar lo que creía entender) y luego de un tiempo me animé a consultar a les **Seniors** del equipo.

El nivel de generosidad de mi equipo es inmenso. Ahí fue cuando las cosas empezaron a acomodarse. Y la importancia de los test cobró forma. Fue a partir de un test que comprendí, o más bien, empecé a comprender aquello que vengo leyendo tanto en [Architecture Patterns With Python](https://www.cosmicpython.com/) como en Test Driven Development.


## Patrón Repository {#patrón-repository}

La palabra patrón es multi significativa en el ámbito de la programación. En este caso, se refiere a una forma de diseñar un sistema, en la cual el modelo de lo que se quiere diseñar está "separado" de los datos reales con los que ese diseño cobra vida a través de una instancia (repositorio) que se encarga de esta comunicación modelo &lt;-&gt; datos. Una expresión más formal sería: Una abstracción sobre de los datos que se guardan y consultan que nos permite separar el modelo de la capa de datos. Una interfaz lógica que interactúa entre el modelo y los datos.
Todo esto son ideas que pueden entenderse, pero esta semana de trabajo me demostró la distancia entre el entendimiento y el conocimiento.

{{< figure src="/images/patron-repository.png" >}}

Entendía los conceptos, mas no los conocía.

Y el acompañamiento de mis compañeres, como la necesidad forzada de hacer test antes de codear hoy me acercaron a conocer un poco más una obra de ingeniería compleja y funcional.

Como dije alguna vez: para mi no hay mejor droga que la que se dispara en mi cuerpo cuando aprendo algo. Y esta semana fue pura falopa.


## Nota {#nota}

-   [Pytest](https://docs.pytest.org/en/7.0.x/) tiene unas cosas muy lindas, entre ellas un plugin de reportee y análisis de por dónde pasa el test en el código mismo [pytest &lt;Acá tu test&gt; \--cov html](https://pytest-cov.readthedocs.io/en/latest/reporting.html)

{{< figure src="/images/pytest-report.png" >}}
