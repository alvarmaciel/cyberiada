+++
title = "Memorias Impuras #4"
lastmod = 2022-02-18T14:04:51-03:00
categories = ["memoriasImpuras", "nerdeadas"]
draft = false
image = "https://c.tenor.com/ofKKbAiVczQAAAAS/gojo-gojo-saotoru.gif"
+++

Sale tarde, porque ayer me quedé festejando mi primer Pull Request en [ShiphHero](https://twitter.com/weareshiphero).
Hoy: Sobre la importancia de dejar hermosa la terminal. Como arreglar macanas en #git eligiendo los commits que quiero pushear. Debugging en #python con #Ipdb: un camino de ida.

<!--more-->

{{< figure src="/images/gojo-gojo-saotoru.gif" >}}

[Gojo Saotoru cortesía de tenor](https://tenor.com/view/gojo-gojo-saotoru-jjk-jujutsu-kaisen-gif-22565583)

-   No podemos dejar de lado la estética de la terminal si trabajamos en ella todos los días. Y con esto quiero decir, dejarla como a uno más le gusta. Una vez hecho esto, levanto mi hermosa terminal, corro [tmux](https://github.com/tmux/tmux/wiki) y me doy cuenta que no tengo más colores. La solución: crear un archivo `.tmux.config` en el directorio raíz con esta línea `set -g default-terminal "screen-256color"` y listo -&gt; [Gracias StackOverflow](https://unix.stackexchange.com/questions/118806/tmux-term-and-256-colours-support)
-   Llegó el día mandé el Pull Request, y me di cuenta que había hecho mal el fetch pasando al Pull Request 21 commits de los 2 que eran los míos. Desastre en puertas. Por suerte tengo un gran Managger que tomó esto como una oportunidad didáctica. El desafío: Pushear otra vez, pero con solo mis 2 commits. No vale hacer otra rama, No vale cancelar el PR. La respuesta fue hacer `git rebase -i HEAD~21` un rebase interactivo sobre los últimos 21 commits levanta tu editor y elegís con `drop` y `pick` que queda y que se va. Works Like Charm -&gt; [Otra vez, gracias StackOverflow](https://stackoverflow.com/questions/36112517/git-exclude-specific-commit-and-push) Pero [chequeen la doc oficial](https://git-scm.com/docs/git-rebase) siempre antes de ejecutar nada.
-   Dejo para el final lo mejor. Tuve una charla inmensa con [@MiltonLn](https://twitter.com/MiltonLn) entre otras cosas sobre debuging y como hacerlo de forma agnóstica del IDE con [Ipython pdb](https://github.com/gotcha/ipdb). Recursos para estudiar
    -   [Esta charla](https://www.youtube.com/watch?v=x8cXLG6zB-Q&t=34s)
    -   [Este texto](https://hasil-sharma.github.io/2017-05-13-python-ipdb/)
    -   Mucha práctica
