+++
title = "Agenda y gestor de tareas para docentes"
lastmod = 2020-05-04T10:07:48-03:00
tags = ["gtd", "orgmode"]
categories = ["nerdeadas"]
draft = true
image = "https://media.giphy.com/media/l2QZUJ0IubBLIf8A0/source.gif"
subtitle = "Tareas y agenda en un solo lugar, sincronizado con el celular"
+++
