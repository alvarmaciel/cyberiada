+++
title = "Memorias Impuras #5"
lastmod = 2022-02-22T19:23:15-03:00
tags = ["python"]
categories = ["memoriasImpuras", "nerdeadas"]
draft = false
image = "https://c.tenor.com/i9iEtTJH-BgAAAAM/miles-morales-spider-man.gif"
+++

Hoy puro #Python ya que fue día de Workshop en [ShipHero](https://shiphero.com):

-   Namespaces
-   Excepciones y
-   ¿Por qué Self?

¿Qué aprendí hoy?

<!--more-->

{{< figure src="/images/miles-morales-spider-man.gif" >}}

-   Este video [The LEGB rule](https://realpython.com/lessons/legb-rule/) tiene un muy buen dato para leer los traceback de #Python en los errores de typo `NameError`. De abajo para arriba son los scopes en los que está buscando el intérprete la definición de la variable. 🚀
-   La declaración  `nonlocal`

<!--listend-->

```python
def f():
    x = 20

    def g():
        x = 40

    g()
    print(f"{x=}")

f()
```

```text
x=20
```

-   En estas funciones: `g()` dentro de `f()` quiere modificar `x` que está definida en `f()`. Pero cuando ejecutamos la función `f()` no se modifica `x` ya que `x` esta definida en el **Enclosing Scope** de `f()`

-   Para modificar `x` desde `g()` necesitamos llamar a la `nonlocal`

<!--listend-->

```python
def f():
    x = 20

    def g():
        nonlocal x
        x = 40

    g()
    print(f"{x=}")

f()
```

```text
x=40
```

No es una buena práctica, no está bueno hacerlo, pero Python nos deja. Así que **RECUERDEN**

{{< figure src="/images/Great-Power-comes-Great-Responsibility.jpg" >}}

-   En Python las excepciones también se pueden usar para el manejo de las señales.
-   **Self** es la referencia a la instancia de ese objeto y este artículo lo deja bien claro [Why explicit self has to stay](http://neopythonic.blogspot.com/2008/10/why-explicit-self-has-to-stay.html). Escrito por el mismísimo Guido Van Rossum.

> [Miles vino de acá](https://tenor.com/view/miles-morales-spider-man-look-down-gif-17729315)
