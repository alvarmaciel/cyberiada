+++
title = "Organizando el trabajo con Orgmode para educadores 1"
lastmod = 2020-03-20T10:29:10-03:00
tags = ["orgmode", "gtd"]
categories = ["nerdeadas"]
draft = true
image = "https://media.giphy.com/media/l2QZUJ0IubBLIf8A0/source.gif"
subtitle = "Como hago para organizar 3 trabajos con 3 roles diferentes como educador"
+++

Esta es una recopilación de artículos, configuraciones y metodologías que uso para organizar las tareas que tengo como docente integrador, docente remoto de pensamiento computacional y referente redagógico del Programa de Pensamiento Computacional de la Fundación Sadosky para el Plan Ceibal.

<!--more-->

Hace años que vengo usando [Org-mode](https://orgmode.org) para gestionar mis tareas y mis escritos, de hecho, este mismo blog está escrito en org-mode. Org-mode es una suerte de complemento (Modo Mayor para ser precisos) de [Emacs](https://www.gnu.org/software/emacs/). Emacs es un editor de texto que tiene la ventaja de ser, ampliable, configurable y en general todos los archivos se producen en texto plano, el formato de texto más sencillo y sustentable que tenemos. Una de sus tantas ventajas es que nos permite escribir sin preocuparnos por el formato del texto y luego exportarlo al tipo de documento que querramos: pdf, doc, html y otros más.
Tiene una curva de aprendizaje elevada. Pero que vale la pena aprender.

{{< figure src="/ox-hugo/orgmodeyweb.png" caption="Figure 1: Ejemplo de uso de emacs + orgmode para este blog:  Así escribo | así se ve en la web" >}}

{{< figure src="/ox-hugo/escribiendoEnEmacs.gif" caption="Figure 2: Así escribo, pueden tener muchos themes claros, oscuros o editar el de ustedes" >}}


## Metodología {#metodología}

Básicamente lo que hago es concentrar en un solo archivo las tareas pendientes, las notas, los links y todo aquello que se me viene a la mente. Esto me libera. Me permite concentrarme en una cosa a la vez y mantener vinculado y funciona como un reservorio de todo ese flujo de cosas que me asaltan cada vez que hago algo. Es como tener un solo cuaderno para todo. Y ese cuaderno digital es un archivo que funciona como bandeja de entrada o inbox en mi caso mi archivo se llama  `inbox.org`

> Tu cerebro es para tener ideas, no para guardarlas - David Allen

Esta frase esta tomada de [un post de Jethro](https://blog.jethro.dev/posts/capturing%5Finbox/) fuente muchas de las cosas que veremos en este artículo. `inbox.org` es donde todo comienza. todo artículo, pensamiento o tarea empieza en el inbox. y capturar esto en él debe ser lo más sencillo posible. Debemos confiar y entregarnos a que, todo sobre lo que tengamos que pensar, esté en el inbox. Esto nos permite concentrarnos al 100% en la tarea en la que estemos.

Las tareas o notas se nos apilaran rápidamente y seguro que aparezcan mientras estamos en medio de algo. El inbox está configurado de forma que pueda capturar rápidamente lo que se me cruza en la cabeza y pueda seguir con lo que estaba haciendo.

{{< figure src="/ox-hugo/workflowSinPensadero.png" caption="Figure 3: Pequeño esquema de como funciona" >}}


### Registrar Todo {#registrar-todo}

Org-mode viene con algo llamado [org-capture](https://orgmode.org/manual/Capture.html), que nos permite guardar notas desde cualquier lado. Lo uso para agregar de forma rápida cualquier cosa a mi inbox.

![](/ox-hugo/capturandoConOrgmode.gif)
Esta captura luego se verá reflejada en mi agenda. Lugar desde donde el cual chequeo y reorganizo mis tareas.

{{< figure src="/ox-hugo/vistaDeAgenda.png" caption="Figure 5: Vista de mi agenda" >}}

Como se ve en la imagen, cada línea o título tiene una palabra clave TODO, NEXT o DONE. Estas palabras son los _estados_ de las tareas, TODO es algo para hacer, pendiente, NEXT es una tarea que está en proceso tiene algún grado de prioridad, DONE son las tareas que están terminadas.


### Agenda {#agenda}

La agenda es donde puedo ver todo lo que capturé, todas mis tareas con fechas límites y fechas u horarios agendados. Las notas, links, ideas o mensajes que me mando a mi mismo. Y también es el lugar donde reviso, reorganizo y me aboco a cada tarea pendiente.
Esta agenda está sincronizada con mi agenda de Google Calendar que es donde me cargan algunas de las reuniones.

Cuando abro la agenda hago una de estas cosas

1.  Chequeo el `inbox.org` y reorganizo las tareas, notas, links o idea.
    1.  Si es una tarea a la que me voy a dedicar en breve o que está en proceso la paso de estado a NEXT
    2.  A veces paso las tareas al proyecto correspondiente solo para tener mi agenda organizada. Después veo si la dejo en TODO o paso a NEXT
2.  Me pregunto ¿en que tarea voy a trabajar? abro esa tarea y me pongo a ella. Muchas veces aprovecho y registro en la misma tarea el tiempo que me toma.
3.  Otras veces marco como hechas las tareas que terminé.

Ademas del archivo `index.org` tengo los archivos `next.org`, `projects.org` `someday.org` y `gcla.or` todo mi GTD (Gets Things Done / Hacer Las Cosas) se organiza en estos archivos. Paso a comentar para que uso cada archivos.

-   `index.org`: Captura todo lo que se pasa por la cabeza
-   `porjects.org`: Donde reorganizo las tareas de proyectos en marcha
-   `next.org`: Donde reorganizo las tareas sueltas que tengo por ahí
-   `someday.org` Esas cosas de las que me quisiera ocupar algún día.
-   `gcal.org` Es donde se sincrinizan las tareas del calendario de google


### Celular {#celular}

Uso [Orgzly](http://www.orgzly.com/) para tener sincronizado todo mis archivos en el celular puedo capturar al inbox, ver y tener las alarmas de los deadlines y eventos agendados.


### Notas y links {#notas-y-links}

Esteoy reformulando lo que venía haciendo. Antes reorganziaba todo en un archivo `link.org` y `notas.org` pero con la lectura de [org-roam](https://org-roam.readthedocs.io/en/master/) todo cambió y estoy en proceso de cambio. Ya llegarán noticias.


### Cómo sigue {#cómo-sigue}

Lo que se viene es cómo configurar todo esto en emacs. Así que estén atentes a los próximos envíos.
