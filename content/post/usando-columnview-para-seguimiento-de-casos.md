+++
title = "Usando column view para hacer seguimiento de casos"
lastmod = 2021-12-29T13:03:31-03:00
tags = ["orgmode"]
categories = ["nerdeadas"]
draft = true
+++

`column view` es una característica de [orgmode](https://orgmode.org) que nos premite organizar la vista de las propiedades de los headline o títulos de nuestro archivo, de una forma en la cual, cada headline es una fila y algunas propiedades son las columnas.
Para que quede claro, a los headlines o títulos se le pueden asignar pares de propiedades que guardan info o setean caracterisiticas de ese headline en particular. Por ejemplo, en mi organización diaria, tengo un archivo en el cual recopilo todas mis tareas. Este archivo `i.org` tiene tres títulos principales: To Do; Sadosky; integraciones. Para hacer una asignación rápida y organizar mi agenda, le asigno el par de propiedades (par porque es la propiedad y su valor) `:CATEGORY: pendientes`

```org
 * To Do
:PROPERTIES:
:CATEGORY: pendientes
:END:

 ** TODO cambiar el blog
```

Entonces, si tenemos un archivo en el cual necesitamos asignar propiedades similares pero con valores disptintos, esta funcionalidad nos ayuda mucho.
