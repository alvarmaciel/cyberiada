+++
title = "Una metodología de toma de notas con Orgmode"
date = 2020-06-17T09:38:00-03:00
lastmod = 2020-06-17T16:54:56-03:00
tags = ["org", "roam", "orgmode"]
categories = ["traducciones"]
draft = false
toc = true
image = "https://media1.tenor.com/images/8ee54ff203d5f5427aef7214cfa3bdd2/tenor.gif?itemid=9135565"
subtitle = "Traducción al vuelo del artículo An Orgmode Note Workflow de Rohit Goswami"
+++

## Aclaración {#aclaración}

Esta es una traducción amateur del artículo [Orgmode Note Workflow](https://rgoswami.me/posts/org-note-workflow) de [Rohit Goswami](https://rgoswami.me). La configuración que acá se muestra es muy similar a la que uso actualmente por lo que puedo confirmar que funciona muy bien

<!--more-->

.[^fn:1]

## Trasfondo {#trasfondo}

Una de las principales y definitivas razones para usar `orgmode` es contar con un mejor manejo de la toma de notas. Muy relacionado con el blogueo o la escritura, el sistema ideal de toma de notas es uno en el cual se nos permita tener un montón de ideas tiradas por ahí y que de alguna manera, tengas acceso a ellas de forma coherente. Este va a ser un post largo, y está en proceso, así que tengan esto en cuenta. Dado que esta es la técnica que usa el autor[^fn:2], a su decir, su filosofía vendrá en otro post. Este método de trabajo está documentado también pero con menos detalle [en los archivos de configuración que pueden ver acá](https://dotdoom.rgoswami.me/config.html#text-3), en la sección[^fn:3] `noteYoda`. Algunas partes de este post incluyen mini videos para dar más claridad[^fn:4].

La metodología o "flujo de trabajo" sería algo como esto[^fn:5]

{{< youtube UWB6ZABRVq0 >}}

## Concepto {#concepto}

Mientras trabajaba en las ideas, fue más útil describir la metodología, el flujo de trabajo que quiero e implementarlo que apoyarme en los enfoques fijos de cada paquete. Así que la idea básica es la que se lista aquí abajo.

### Gestión de referencias bibliográficas {#gestión-de-referencias-bibliográficas}

La gestión de las referencias es una de las principales razones para considerar una configuración en texto plano. Las opciones más comunes son:

Mendeley
: Esta es una muy buena opción, y la más amigable con los celulares de todas estas. Lamentablemente los precios no son tan amigables así que tuve que dejarlo pasar.

Jabref
: Este es bueno, pero es maś un sistema de gestión entre pares, que funciona muy bien para eso. El hecho de que esté basado en Java fue un problema para mi.

Zotero
: Este es el que uso personalmente y que recomiendo. Más sobre el vendrá en un post posterior.

## Notas {#notas}

La idea es ser capaz de crear notas de todo tipo y contenido. Específicamente de papers o libros, así como páginas webs. Esto requiere un sistema separado para cada caso que se describe de la siguiente forma:

Motor de búsqueda
: El motor de búsqueda es la clave, tanto en términos de accesibilidad como escalabilidad. Asumimos que habrá muchas notas, y que tendrán un amplia variedad de contenidos. La interfase de búsqueda debe simplemente permitirnos limitar a los candidatos de la búsqueda de manera significativa.

Representación contextual
: Este aspecto de la metodología lidia con las representaciones, que debe trascender el uso de etiquetas y categorías. En particular, sería lindo ser capaces de visualizar el flujo de ideas, cada una representada como una nota.

Backlinks
: Por backlinks, nos referimos a la habilidad de linkear a un `pdf` o a una página web con una clave única de manera que las notas puedan ser agregadas o removidas a voluntad.

Guardado
: En realidad, no forma parte del flujo de trabajo de la misma manera, ya que se manejará a nivel del sistema, no vale la pena, que en este flujo de trabajo Zotero sea utilizado para exportar y mantener actualizado un archivo **bib** maestro, ya que las notas en sí son la versión de control.[^fn:6]

Los conceptos anteriores serán manejados por los siguientes paquetes

| Concepto  | Paquete                      | Nota                                                      |
| --------- | ---------------------------- | --------------------------------------------------------- |
| Búsqueda  | deft                         | Tiene una muy buena interfase                             |
| Contexto  | org-roam                     | Nos permite exportar mapas mentales con graphiz           |
| Backlinks | org-roam, org-ref, org-noter | Cubre websites, bibliografías y pdfs/epub respectivamente |

un componente clave en esta metodología es facilitada por el fabuloso `org-roam-bibtex` [o ORB](https://github.com/org-roam/org-roam-bibtex). La idea básica es asegurar templates (plantillas) significativas que se interpolen suavemente con `org-roam`, `org-ref`, `helm-bibtex` y `org-capture`.

### Variables básicas {#variables-básicas}

Dado los paquetes que estamos usando, estas son algunas configuraciones de variables, deben ir en orden.

```emacs-lisp
(setq
   org_notes (concat (getenv "HOME") "/Git/Gitlab/Mine/Notes/")
   zot_bib (concat (getenv "HOME") "/GDrive/zotLib.bib")
   org-directory org_notes
   deft-directory org_notes
   org-roam-directory org_notes
   )
```

## Búsqueda {#búsqueda}

Para configurar la búsqueda, establecer `deft` en `doom-emacs`. Agregando `+deft` en mi `init.el` esto funcionó de maravillas. Para quienes no usan `doom`[^fn:7], la configuración siguiente tendría que ser suficiente.

```emacs-lisp
(use-package deft
  :commands deft
  :init
  (setq deft-default-extension "org"
        ;; de-couples filename and note title:
        deft-use-filename-as-title nil
        deft-use-filter-string-for-filename t
        ;; disable auto-save
        deft-auto-save-interval -1.0
        ;; converts the filter string into a readable file-name using kebab-case:
        deft-file-naming-rules
        '((noslash . "-")
          (nospace . "-")
          (case-fn . downcase)))
  :config
  (add-to-list 'deft-extensions "tex)
  )
```

Para más información sobre la configuración por defecto de `doom-emacs`, pueden chequear el [repositorio en github](https://github.com/hlissner/doom-emacs/search?q=deft&unscoped%5Fq=deft) del proyecto. El otro aspecto de la interacción con las notas es a través de la interfase de `org-roam` y esto es cubierto más adelante.

## Bibliografía. {#bibliografía-dot}

Dado que vamos a estar usando `org-ref`, no tiene sentido cargar o trabajar con el módulo `+biblio` en este momento. Por lo tanto esta sección es `doom` agnóstica. La herramienta básica del manejo bibliográfico desde el lado de `emacs` son el venerable `helm-bibtex` ([acá su repo](https://github.com/tmalsburg/helm-bibtex)) y `org-ref` ([acá su repo](https://github.com/jkitchin/org-ref/)). En orden de hacer esta guía completa, también voy a describir la configuración de [Zotero](https://www.zotero.org/) que tengo.

### Zotero {#zotero}

Sin entrar en profundidad, los requerimientos básicos son:

- [x] Zotero
- [x] La extensión [better bibtex extension](https://retorque.re/zotero-better-bibtex/)

La idea es tener un archivo `.bib` principal en alguna ubicación cómoda y que se pueda sincronizar automáticamente. Para hacernos la vida más sencilla, acá tienen un pequeño video de los pasos a seguir.

{{< youtube iDRpIo7mcKE >}}

## Helm-bibtex {#helm-bibtex}

Este [venerable paquete](https://github.com/tmalsburg/helm-bibtex) es muy bueno para hacer interfase con una amplia variedad de formatos externos de gestores bibliográficos.

```emacs-lisp
(setq
 bibtex-completion-notes-path "/home/haozeke/Git/Gitlab/Mine/Notes/"
 bibtex-completion-bibliography "/home/haozeke/GDrive/zotLib.bib"
 bibtex-completion-pdf-field "file"
 bibtex-completion-notes-template-multiple-files
 (concat
  "#+TITLE: ${title}\n"
  "#+ROAM_KEY: cite:${=key=}\n"
  "* TODO Notes\n"
  ":PROPERTIES:\n"
  ":Custom_ID: ${=key=}\n"
  ":NOTER_DOCUMENT: %(orb-process-file-field \"${=key=}\")\n"
  ":AUTHOR: ${author-abbrev}\n"
  ":JOURNAL: ${journaltitle}\n"
  ":DATE: ${date}\n"
  ":YEAR: ${year}\n"
  ":DOI: ${doi}\n"
  ":URL: ${url}\n"
  ":END:\n\n"
  )
 )
```

Las usuarias de `doom-emacs` tal vez quieran envolver el código anterior en una linda expresión `after! org-ref`, pero en realidad no importa.

### Explicación {#explicación}

Para desglosar aspectos del fragmento de configuración anterior:

- la plantilla incluye la función `orb-process-file-field` para poder seleccionar el `pdf` que se usará con `org-noter`.
- Se especifica que el campo `file` trabaje con el archivo `.bib` generado por Zotero.
- `helm-bibtex` habilita que cualquiera de las claves en un archivo `.bib` pueda ser usada en la plantilla, y mientras maś expresiva más útil.
- La clave `ROAM_KEY` es definida para asegurar que los backlinks de citas trabajen de forma correcta con `org-roam`
- Como prefiero tener un archivo de notas por `pdf`, configuré solo la variable `bibtex-completion-notes-template-multiple-files`.

## Org-ref {#org-ref}

Como se dijo arriba, esto hace que las citas en `orgmode` sean más significativas.

```emacs-lisp
(use-package org-ref
    :config
    (setq
         org-ref-completion-library 'org-ref-ivy-cite
         org-ref-get-pdf-filename-function 'org-ref-get-pdf-filename-helm-bibtex
         org-ref-default-bibliography (list "/home/haozeke/GDrive/zotLib.bib")
         org-ref-bibliography-notes "/home/haozeke/Git/Gitlab/Mine/Notes/bibnotes.org"
         org-ref-note-title-format "* TODO %y - %t\n :PROPERTIES:\n  :Custom_ID: %k\n  :NOTER_DOCUMENT: %F\n :ROAM_KEY: cite:%k\n  :AUTHOR: %9a\n  :JOURNAL: %j\n  :YEAR: %y\n  :VOLUME: %v\n  :PAGES: %p\n  :DOI: %D\n  :URL: %U\n :END:\n\n"
         org-ref-notes-directory "/home/haozeke/Git/Gitlab/Mine/Notes/"
         org-ref-notes-function 'orb-edit-notes
    ))
```

Un aspecto esencial de esta configuración es que la mayoría del trabajo pesado en relación a las notas se reducen a `helm-bibtex`.

### Explicación {#explicación}

Desglosando aspectos del fragmento de configuración anterior:

- la función `org-ref-get-pdf-filename-function` simplemente usa la configuración de `helm-bibtex` para encontrar el `pdf`
- El directorio por defecto de la bibliografía y las notas son establecidos en el mismo lugar que todos los archivos `org-roam`, para facilitas esa jerarquía plana.
- La función `org-ref-notes-function` simplemente asegura que, tal como la configuración de `helm-bibtex`, esperamos un archivo por `pdf`, y vamos a usar el nuestra plantilla de `org-roam` en lugar del de `org-ref` o `helm-bibtex`.

Notarán que por alguna razón, los especificadores de formato para `org-ref` **no** son las claves en `.bib` sino que son las siguientes[^fn:8]

```bash
In the format, the following percent escapes will be expanded.
%l   The BibTeX label of the citation.
%a   List of author names, see also `reftex-cite-punctuation'.
%2a  Like %a, but abbreviate more than 2 authors like Jones et al.
%A   First author name only.
%e   Works like %a, but on list of editor names.  (%2e and %E work as well)
It is also possible to access all other BibTeX database fields:
%b booktitle     %c chapter        %d edition    %h howpublished
%i institution   %j journal        %k key        %m month
%n number        %o organization   %p pages      %P first page
%r address       %s school         %u publisher  %t title
%v volume        %y year
%B booktitle, abbreviated          %T title, abbreviated
%U url
%D doi
%S series        %N note
%f pdf filename
%F absolute pdf filename
Usually, only %l is needed.  The other stuff is mainly for the echo area
display, and for (setq reftex-comment-citations t).
%< as a special operator kills punctuation and space around it after the
string has been formatted.
A pair of square brackets indicates an optional argument, and RefTeX
will prompt for the values of these arguments.
```

## Indexado de notas {#indexado-de-notas}

Esta parte de la metodología o flujo de trabajo está construida en los conceptos más conocidos como el [método Zettelkasten](https://www.zettelkasten.de/). Pueden encontrar más sobre la filosofía detrás de `org-roam` en este [link](https://org-roam.readthedocs.io/en/latest/).

### Org-roam {#org-roam}

La primer parte de esta interfase es esencialmente, tan solo, la configuración de `doom-emacs`, adaptada para aquellos que aún no creen en le lado oscuro.

```emacs-lisp
(use-package org-roam
  :hook (org-load . org-roam-mode)
  :commands (org-roam-buffer-toggle-display
             org-roam-find-file
             org-roam-graph
             org-roam-insert
             org-roam-switch-to-buffer
             org-roam-dailies-date
             org-roam-dailies-today
             org-roam-dailies-tomorrow
             org-roam-dailies-yesterday)
  :preface
  ;; Set this to nil so we can later detect whether the user has set a custom
  ;; directory for it, and default to `org-directory' if they haven't.
  (defvar org-roam-directory nil)
  :init
  :config
  (setq org-roam-directory (expand-file-name (or org-roam-directory "roam")
                                             org-directory)
        org-roam-verbose nil  ; https://youtu.be/fn4jIlFwuLU
        org-roam-buffer-no-delete-other-windows t ; make org-roam buffer sticky
        org-roam-completion-system 'default

  ;; Normally, the org-roam buffer doesn't open until you explicitly call
  ;; `org-roam'. If `+org-roam-open-buffer-on-find-file' is non-nil, the
  ;; org-roam buffer will be opened for you when you use `org-roam-find-file'
  ;; (but not `find-file', to limit the scope of this behavior).
  (add-hook 'find-file-hook
    (defun +org-roam-open-buffer-maybe-h ()
      (and +org-roam-open-buffer-on-find-file
           (memq 'org-roam-buffer--update-maybe post-command-hook)
           (not (window-parameter nil 'window-side)) ; don't proc for popups
           (not (eq 'visible (org-roam-buffer--visibility)))
           (with-current-buffer (window-buffer)
             (org-roam-buffer--get-create)))))

  ;; Hide the mode line in the org-roam buffer, since it serves no purpose. This
  ;; makes it easier to distinguish among other org buffers.
  (add-hook 'org-roam-buffer-prepare-hook #'hide-mode-line-mode))


;; Since the org module lazy loads org-protocol (waits until an org URL is
;; detected), we can safely chain `org-roam-protocol' to it.
(use-package org-roam-protocol
  :after org-protocol)


(use-package company-org-roam
  :after org-roam
  :config
  (set-company-backend! 'org-mode '(company-org-roam company-yasnippet company-dabbrev)))
```

Una vez más, para más detalles, chequeen el [repo de github](https://github.com/hlissner/doom-emacs/search?q=roam&unscoped%5Fq=roam).

### Org-Roam-bibtex {#org-roam-bibtex}

La configuración requerida es la siguiente:

```emacs-lisp
 (use-package org-roam-bibtex
  :after (org-roam)
  :hook (org-roam-mode . org-roam-bibtex-mode)
  :config
  (setq org-roam-bibtex-preformat-keywords
   '("=key=" "title" "url" "file" "author-or-editor" "keywords"))
  (setq orb-templates
        '(("r" "ref" plain (function org-roam-capture--get-point)
           ""
           :file-name "${slug}"
           :head "#+TITLE: ${=key=}: ${title}\n#+ROAM_KEY: ${ref}

- tags ::
- keywords :: ${keywords}

\n* ${title}\n  :PROPERTIES:\n  :Custom_ID: ${=key=}\n  :URL: ${url}\n  :AUTHOR: ${author-or-editor}\n  :NOTER_DOCUMENT: %(orb-process-file-field \"${=key=}\")\n  :NOTER_PAGE: \n  :END:\n\n"

           :unnarrowed t))))
```

La mayor parte de la configuración es esencialmente una nueva plantilla. Al igual que en `helm-bibtex`, [ORB](https://github.com/org-roam/org-roam-bibtex) permite tomar claves arbitrarias de `.bib`

## Org Noter {#org-noter}

El aspecto final del flujo de trabajo con un `pdf` es simplemente, asegurarnos que cada `pdf` es asociado con las notas. La filosofía de `org-noter` se [describe mejor acá](https://github.com/weirdNox/org-noter). Se requieren algunos ajustes menores para tener esto funcionando también con `interleave`.

```emacs-lisp
(use-package org-noter
  :after (:any org pdf-view)
  :config
  (setq
   ;; The WM can handle splits
   org-noter-notes-window-location 'other-frame
   ;; Please stop opening frames
   org-noter-always-create-frame nil
   ;; I want to see the whole file
   org-noter-hide-other nil
   ;; Everything is relative to the main notes file
   org-noter-notes-search-path (list org_notes)
   )
  )
```

Evidentemente, de esta configuración, queda claro que he decidido usar [org-noter](https://github.com/weirdNox/org-noter) en lugar del más comúnmente descripto [interleave](https://github.com/rudolfochrist/interleave) ya que tiene mejor soporte para trabajar con múltiples documentos linkeados a un archivo.

## Org-Protocol {#org-protocol}

Solo cubriré lo mínimo relacionado con el uso de `org-capture` aquí, porque eventualmente tengo la intención de manejar muchos más casos con [orca](https://github.com/abo-abo/orca). Obsérvese que esta parte de la metodología tiene más que ver con usar `org-roam` con páginas webs que con archivos `pdf`.

### Templates {#templates}

Esta parte puede ser complicada pero voy tratar de traer acá la configuración mínima de `org-protocol`.

```emacs-lisp
;; Actually start using templates
(after! org-capture
  ;; Firefox and Chrome
  (add-to-list 'org-capture-templates
               '("P" "Protocol" entry ; key, name, type
                 (file+headline +org-capture-notes-file "Inbox") ; target
                 "* %^{Title}\nSource: %u, %c\n #+BEGIN_QUOTE\n%i\n#+END_QUOTE\n\n\n%?"
                 :prepend t ; properties
                 :kill-buffer t))
  (add-to-list 'org-capture-templates
               '("L" "Protocol Link" entry
                 (file+headline +org-capture-notes-file "Inbox")
                 "* %? [[%:link][%(transform-square-brackets-to-round-ones \"%:description\")]]\n"
                 :prepend t
                 :kill-buffer t))
)
```

## Conclusiones {#conclusiones}

En este punto, muchos pueden argumentar que dado que al final, solo se llama a una plantilla, definir el resto no tiene sentido. Puede que tengan razón, sin embargo, así es como evolucionó mi configuración. Siéntanse libres de canibalizar esto para sus beneficios personales. Eventualmente planeo expandir esto en algo con `org-journal` también, pero no ahora mismo.

[^fn:1]: fuente la imagen: [Tenor](https://tenor.com/view/writing-gif-9135565)
[^fn:2]: Rohit Goswami es el autor, [acá está su página de inicio.](https://rgoswami.me/)
[^fn:3]: Es una referencia a la mascota del autor
[^fn:4]: Grabado con [SimpleScreenRecorder](https://www.maartenbaert.be/simplescreenrecorder/) cortado y editado con [LosslessCut](https://github.com/mifi/lossless-cut), subido a [Youtube](https://www.youtube.com/) y embebido con [Hugo Shortcode](https://gohugo.io/extras/shortcodes/#youtube)
[^fn:5]: Este video usa `org-ref-notes-function-many-files` como la función `org-ref-notes-function` así que los templates se ven un poco diferentes.
[^fn:6]: Por alguna razón un montón de artículos online sugieren Dropbox para mantener sincronizadas las notas, lo cual no tiene sentido para mi, es mejor siempre tener un controlador de versiones e ignorar las reglas.
[^fn:7]: Lo que probaría claramente que las tentaciones del lado oscuro no tuvieron efecto en la guerra sagrada del editor de texto
[^fn:8]: Estos provienen de la [documentación de org-ref](https://github.com/jkitchin/org-ref/blob/875371a63430544e446db7a76b44b33c7e20a8bd/org-ref-utils.el)
