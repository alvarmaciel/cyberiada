+++
title = "Porqué hacerme esto a mi edad"
lastmod = 2019-03-26T12:03:12-03:00
tags = ["debian"]
categories = ["nerdeadas"]
draft = false
image = "images/debianando.jpg"
+++

Soy usaurio de GNU/Linux desde 1999 más o menos. Mi primer distribución fue un RedHat 3? tal vez. Una pila de disquetes de 3 1/2 y un sistema que me tiraba una consola para que incie unas X que ni sabía para que servían. Desde ahí hasta hoy pasó mucha agua bajo el puente y en un devenir de distribuciones, siempre terminé usando Debian. Ahora, un poco más viejito, con unos conocimentos de mi mismo más inestables y aprovechando que me vine a vivir a un lugar más tranquilo, puedo poner manos a la obra para devolverle a Debian un poco de todo lo que me dió. aprendiendo a mantener paquetes y de paso aprendiendo un poco más de la lógica interna de una comunidad que se instaló en mi vida.

Porque obviar lo que representa para un usaurio de mi edad, 20 años de uso sostenido de un SO, uso que me permitió trabajar con lo que aprendí y disfrutar de sus delicadezas técnicas es algo injusto.

Así que me embarco en este desafío. Voy a empeazar leyendo la Nueva guia para Debian Manteiners y trataré de mantener este espacio del blog como un anotador personal, donde poder volcar que significan todas esas abreviaciones.

{{< figure src="/ox-hugo/debianando.jpg" >}}
