+++
title = "Como exportar desde orgmode a pdf en castellano"
date = 2024-02-26T10:42:00-03:00
lastmod = 2024-02-26T12:31:45-03:00
tags = ["emacs", "orgmode", "orgroam"]
categories = ["nerdeadas"]
draft = false
image = "https://media4.giphy.com/media/v1.Y2lkPTc5MGI3NjExejQ4OHZjYzg5OWtneHJnNnR6bjhqZTBiNGIxZzAxYjQzanI4YXZwayZlcD12MV9pbnRlcm5hbF9naWZfYnlfaWQmY3Q9Zw/LHZyixOnHwDDy/giphy.gif"
subtitle = "Y tener todo en un solo lugar"
+++

Dado que empecé a estudiar una carrera formal, creo necesario adaptar mi uso de org-mode a publicacion de mis archivos en formato pdf.

<!--more-->

![](https://media4.giphy.com/media/v1.Y2lkPTc5MGI3NjExejQ4OHZjYzg5OWtneHJnNnR6bjhqZTBiNGIxZzAxYjQzanI4YXZwayZlcD12MV9pbnRlcm5hbF9naWZfYnlfaWQmY3Q9Zw/LHZyixOnHwDDy/giphy.gif)

Generalmente mi flujo de estudio es el siguente:

-   Tomo notas en papel o usando org-roam.
-   No exporto mis notas a ningún formato.

Ahora, para presentar trabajos prácticos y evaluaciones voy a necesitar escribir y generar PDF. Para mí la mejor opción es la siguente:

-   Escribo, incluyebdo codigo, salidas de codigo y formulas LaTex
-   Un comando me exporta todo a PDF 😁

Esto en la práctica se ve así, estas son notas tal cual las escribo

```orgmode
** Modelos lineales usando orgmode {#modelos-lineales-usando-orgmode}
- \(y = 3x-2\)

#+LABEL: Tabla de valores
#+CAPTION: Tabla 1
  | x |  0 | 1 | 2 |
  | y | -2 | 1 | 4 |

#+BEGIN_SRC python :results file :exports both
import matplotlib.pyplot as plt

# Define the data
x = [0, 1, 2]
y = [-2, 1, 4]

# Plot the data
plt.figure(figsize=(8, 6))
plt.plot(x, y, marker='o', linestyle='-', color='b')

# Add title and labels
plt.title('Plot of y = 3x - 2')
plt.xlabel('x')
plt.ylabel('y')

# Show grid
plt.grid(True)

# Save the plot as a PNG file
plt.savefig('modelos_lineales.png')

# Display the plot in a way that Org mode can capture
plt.close()  # This ensures the figure is closed after saving and before Org tries to display it
return('modelos_lineales.png')
#+END_SRC

#+RESULTS:
[[file:modelos_lineales.png]]
#+end_src
```

Acá hay tres elementos:

1.  Un encabzado para exportar y renderizar los campos acutoáticos en castellano
2.  Una ecuacion encerrada en `\\(\\)`. Así la ecuación se exportará a LaTex y puedo tener lindas representaciones
3.  Una tabla que luego es exportada con su formato
4.  El código que se ejecuta en ese archivo de orgomode y me da su salida, en este caso un grafico.

Así se ve en el editor con la salida del codigo python, el gráfico ploteado

![](/ox-hugo/2024-02-26-captura_snippet.png)

Con solo agregarle este header al inicio del archivo o el sub item logramos que al momento de exportar, los campos automáticos se rendericen en castellano

```orgmode
#+LATEX_HEADER: \usepackage[spanish, es-noquoting]{babel}
#+LANGUAGE: es
```

El pdf final lo obtengo con `C-c C-e` esta captura es del pdf generado. Se puede ver que tiene un campo `Índice` vacío porque no tengo sub items y que la tabla 1 tiene un encabezado `Cuadro 1: Tabla 1` y el grafico.

![](/ox-hugo/2024-02-26-captura_pdf.png)

Si quieren pueden ver el archivo completo [acá](/ox-hugo/20240213085921-apoyo_en_matematica.pdf)

Imagen via [Giphy](https://giphy.com/gifs/computer-working-cat-LHZyixOnHwDDy)
