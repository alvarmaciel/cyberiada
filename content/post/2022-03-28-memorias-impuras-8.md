+++
title = "Memorias Impuras #8"
lastmod = 2022-03-28T19:57:12-03:00
tags = ["python", "OOP"]
categories = ["memoriasImpuras", "nerdeadas"]
draft = false
image = "https://alvarmaciel.gitlab.io/cyberiada/images/camp-fire.gif"
+++

Estas notas impuras vienen cargadas del [PyCamp](https://wiki.python.org.ar/PyCamp/2022/). El campamento de la comunidad de [Python Argentina](https://www.python.org.ar/).
fue tan, pero tan requete  bueno que todavía lo proceso, con el tiempo le voy a dar forma de post.
Mientras, unas notas de cosas aprendidas de [este video](https://www.youtube.com/watch?v=CvQ7e6yUtnw) que me recomendaron

<!--more-->

{{< figure src="/images/camp-fire.gif" >}}


## Identidad y DataClasses {#identidad-y-dataclasses}

-   Dataclasses nos permite crear clases de datos, esto es un tipo de clase que representa solo datos y estos representan su identidad. Este tipos de clase es genial para Value Objects (ver [Architecture Patterns With Python](https://cosmicpython.com)).
-   Usalmente este tipo de Value Objects tienen datos que representan su identidad y se las hace inmutables.

-   Nos ayuda a crear mejores _Data Oriented Classes_

-   Definen el método `__repr__` por nosotros con la data de los atributos

-   Si este método está compuesto por los valores de los atributos, entonces los los valores los que representan al objeto instanciado. Así dos objetos instanciados con los mismos valores mantienen la identidad. Esto se representa muy claro con las monedas.

-   Por ejemplos, solo podríamos evaluar la identidad de  dos billetes de la misma denominación y distinto valor

<!--listend-->

```python
from dataclasses import dataclass

@dataclass
class Plata:
    moneda: str
    valor: int

def test_igualdad(billete1, billete2):
    try:
        assert billete1 == billete2
        print("Son de la misma moneda")
    except AssertionError:
        print("Son de monedas diferentes")

test_igualdad(Plata("peso", 10), Plata("peso", 20))
```

```text
Son de la misma moneda
```

-   Mismo Valor y diferente denominación

<!--listend-->

```python

test_igualdad(Plata("peso", 10), Plata("soles", 10))
```

```text
Son de monedas diferentes
```


## Features de DataClassses {#features-de-dataclassses}

-   Se pueden definir valores por defecto
-   Se pueden generar clases inmutables a través de los parámetros del decorador `@dataclass(frozen=True)`
-   Factory function: Esto nos sirve por ejemplo para dar valores pro defecto que no se solapen en las instanciaciones de los objetos.
-   de esta forma podemos asignar funciones como generadores de valores para los atributos. Por ejemplo

<!--listend-->

```python
import random
import string
from dataclasses import dataclass, field

def generar_id()->str:
    return "".join(random.choices(string.ascii_uppercase, k=12))

@dataclass(frozen=True)
class Socie:
    nombre: str
    direccion: str
    direcciones_de_mail: list[str] = field(default_factory=list)
    id: str = field(default_factory= generar_id)

juana = Socie(nombre="Juana", direccion="La Barca")
print(juana)
```

```text
Socie(nombre='Juana', direccion='La Barca', direcciones_de_mail=[], id='DWABHYRVOTWI')
```

-   También puede restringirse las inicializaciones del default_factory con `init=False`
-   Puede usarse el método `__post_init__` para otorgar valor a atributos después de la inicialización de los dataclass objects `_un_atributo_con_valores_postumos: str = field(init=False)` luego deberá definirse `__post_init__` para ese atributo.
-   También pude ocultarse la representación de ese atributo de la clase para que no forme parte de su identidad: Supongamos que agregamos a la data clase Plata un símbolo. Y este es una propiedad privada y no forma parte de la identidad del objeto ya que múltiples monedas tienen el mismo símbolo:

<!--listend-->

```python
from dataclasses import dataclass, field

@dataclass
class Plata:
    moneda: str
    valor: int
    _simbolo: str = field(init=False, repr=False)

    def __post_init__(self) -> None:
        if "Peso" in self.moneda.split():
            self._simbolo = "$"
        else:
            self._simbolo = "$$"

def test_igualdad(billete1, billete2):
    try:
        assert billete1 == billete2
        return f'Son la misma moneda'
    except AssertionError:
        return f"Son monedas diferentes"


billete1 = Plata("Peso Fuerte", 10)
billete2 = Plata("Peso Solar", 20)
billete3 = Plata("Sol Andino", 10)
print(f'''Simbolo del "{billete1.moneda}": "{billete1._simbolo}".
Símbolo del {billete2.moneda}: {billete2._simbolo}
Símbolo del {billete3.moneda}: {billete3._simbolo}''')

print(f'El {billete1.moneda} y el {billete2.moneda}. {test_igualdad(billete1, billete2)}')
```

```text
Simbolo del "Peso Fuerte": "$".
Símbolo del Peso Solar: $
Símbolo del Sol Andino: $$
El Peso Fuerte y el Peso Solar. Son monedas diferentes
```
