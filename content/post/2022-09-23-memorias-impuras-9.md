+++
title = "Memorias Impuras #9"
date = 2022-09-23T18:37:00-03:00
lastmod = 2022-09-29T09:59:12-03:00
tags = ["python", "tdd"]
categories = ["memoriasImpuras", "nerdeadas"]
draft = false
image = "https://alvarmaciel.gitlab.io/cyberiada/images/midoriya-izuku-anime-stud.gif"
+++

La memoria es un objeto complejo. Y suponer que todos los días aprendemos algo entraña una trampa.
Establece que los aprendizajes son procesos veloces. O al menos, suficientemente veloces como para aprender todos los días algo.
Si algo me enseño la docencia, es que el aprendizaje es un proceso lento.

Y lento es normal.

El tema de hoy

-   TDD y Pytest... y un poco de Gherkin

<!--more-->

{{< figure src="/images/midoriya-izuku-anime-stud.gif" >}}

Abro este post reflexionado en lo ilusoria que fue mi convicción en que podría escribir todos los días algo que haya aprendido. En lugar de eso, estas memorias impuras se irán llenando a medida que sienta que aprendí algo. Y que tenga tiempo de escribirlo.

> -   Update: Gracias Carlos Crespo por tus preguntas, mejoramos la conclusión sobre `try-except`


## El problema {#el-problema}

Tengo que escribir una mutación graphql que dispara un método que se comunica (usando un servició ya escrito) con otra API y actualiza datos en ella. Hice una prueba de concepto, la cosa funcionó de forma aislada, sin más que la integración mínima y necesaria con mi sistema. Ahora para escribir el métdo integrado al sistema, me propuse hacerlo comenzando con los test. A mitad de camino escribí esto a unos amigos:

> Estoy pensando algo sobre TDD que quiero compartir por acá. Aplicar TDD desde el inicio de un feature determina aspectos del diseño del feature. Esta determinación esta dada por lo que conozco de la librería de test que uso. O por lo que saben y comparten los colegas. Este conocimiento puede ser muy aprovechado en los review de código.
>
> Entonces. Si empezás a desarrollar features, y queres hacer TDD, el tiempo que inviertas en modelar escenarios para tus tests va a ser re invertido en ampliar los conocimientos sobre diseño.

Cuando lo escribí (hace unas dos semanas) me daba cuenta lo poco que se sobre Pytest, Mocks, Patch y Exceptions.

Así las cosas, algo bloquado y luego de unas revisiones profundas, me puse a modelar por fuera del código que tenía que modificar, una implementación sencilla de lo que quería testear.

Osea, me fui del repo, containers y demás, abrí una [Jupyter Notebook](https://jupyter.org/) y me puse a tratar de reproducir algunos elementos estructurales de aquello que debía testear y modelar (a esta altura, sólo el método) y esa práctica, ese tiempo invertido en descontextualizar, me permitío desbloquearme y empezar a dar respuestas al problema que tenía. A saber: no podía levantar una exceptción en el lugar adecuado para probar el flujo de mi código.

Nada de lo que aprendí pudo haber sido posible sin la inmensa generosidad del equipo de trabajo #HermesSosLoMas

{{< figure src="/images/my-hero-thank-you.gif" >}}


## La Notebook {#la-notebook}

Es una pena no poder mostrarles un time-lapse de lo que fui haciendo. Pero voy a intentarlo:

Primero, escribí un listado de comportamientos que esperaba de los métodos que quería testear.
Esto lo escribí en Gherkin. Un lenguaje específico para representar escenarios y comportamientos esperados. Forma parta de lo que viene a llamarse Desarrollo Dirigido por Comportamiento. [Más info acá](https://profile.es/blog/que-es-gherkin/)
No era la intencíon hacer BDD, sino solo usar sus elementos para ordenarme.
Mientras lo escribia pude ir abstrayendo las relaciones del modelo real

-   Given a Class `PrintMessage`
-   Given `PrintMessage` live in `app_example_1/module_1/submodule_1_1/submodule_1_1_1`
-   Given `PrintMessage` have a method called `print_message`
-   Given `PrintMessage.print_message` takes an String parameter
-   When `PrintMessage.print_message` is called with a String argument
-   Then the argument return the string
-   When `PrintMessage.print_message` is called with non String argument
-   Then `PrintMessage.print_message` raise an exception

En resumen: Una clase con un método que devuelva el string que se le pasa, y luego otra clase que implente este método y convierta el string en mayúscula.

Así comencé los tests en la Notebook

```python
import pytest
from unittest.mock import patch
from unittest.mock import Mock
from app_example_1.module_1.submodule_1_1.submodule_1_1_1.print_messages import PrintMessage
```

{{< figure src="/images/module_not_found.png" >}}

Un hermoso error, todavía no tenía la estructura de directorios ni el archivo `print_messages.py` y esta es la escencia de TDD.

Después escribí el primer test propiamente dicho, y lo quise correr en la notebook. Obvio no obtuve el resultado esperado. Ejecutar la celdas en la notebook no ejecuta los test. Para ejecutar los test en la notebook usé [ipytest](https://github.com/chmp/ipytest) (gracias equipo otra vez). Esta fue la primer función de testeo

```python
def test_app_example_has_a_method_print_message():
    # Given an object of Class PrintMessage
    test_object = PrintMessage()
    # When the method print_message is called, efectivily is called
    with patch('app_example_1.module_1.submodule_1_1.submodule_1_1_1.print_messages.PrintMessage.print_message')as mocked_method:
        test_object_of_PrintMessage.print_message("hola")
        mocked_method.assert_called_once()
        mocked_method.assert_called_once_with("hola")
```

y este el Error correspondiente

{{< figure src="/images/name_error.png" >}}

escribí el archivo y su clase

```python
class PrintMessage:

    def print_message(self, arg: str) -> str:
        return arg
```

y el test pasó, luego el test para probar como levantar la Excepcion.

En esta instancias tuve mis encuentros con el problema que enfrentaba en el código real. Pero al estar en un contexto diferente, más controlado, pude probar y probar hasta entender que hacía mal.

{{< figure src="/images/did_not_raise.png" >}}

Y llegó el desbloqueo y esa sensación de aprendizaje que tanto me gusta. Pude pensar como implementar una solución para el código en cuestión.

{{< figure src="/images/tres_passes.png" >}}


## Conclusiones {#conclusiones}

-   Más que hoy aprendí. Esta semana/mes/añó estoy aprendiendo.
-   Patchar lo que querés como vuelta de lo que testeas.
-   Las Excepciones, si las gestiono con `try-except` las tengo que levantar con `rasie` (parece algo obvio y no lo es) sino hago nada, son capturadas por la clausula `except` y nada pasa con ellas, a menos que hagas algo, loguear el error, devolver un valor o que se ejecute la excepcion cortando el flujo (dependiendo del tipo de escepción)
-   Descontextualizar y abstraer ayudan a pensar el problema o aprender lo que querés implentar
-   Se como Hermes.

Les dejo el link a la [Notebook final](https://github.com/alvarmaciel/100-days-of-code/blob/main/python_things/notebooks/memorias_impuras.ipynb) y en ese repo también pueden encontrar el código de las clases.
