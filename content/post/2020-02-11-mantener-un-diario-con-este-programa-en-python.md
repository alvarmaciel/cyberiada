+++
title = "Mantener un diario de tus actividades con este programa hecho en python"
lastmod = 2020-02-11T16:53:04-03:00
tags = ["python", "productividad"]
categories = ["traducciones"]
draft = false
image = "https://opensource.com/sites/default/files/styles/image-full-size/public/lead-images/notebook-writing-pen.jpg?itok=uA3dCfu_"
subtitle = "Jrnl crea un diario de sus actividades diarias explorable,calendarizado, exportable y (si quieren) encriptable. Profundizando en la octava entrega en nuestra serie: 20 formas de ser más productivo usando Herramientas Open Source en el 2020"
+++

Traducción del artículo [Keep a journal of your activities with this Python program](https://opensource.com/article/20/1/python-journal) de [Kevin Sonney](https://opensource.com/users/ksonney) en [OpenSource.com](https://opensource.com)

<!--more-->

{{< figure src="/images/notebook-writing-pen.jpg" caption="Figure 1: Crédito de la imagen: Pexels, via Pixabay. CC0." >}}

El año pasado, les traje 19 días de nuevas (para ustedes) herramientas de productividad para el 2019. Esta vez, estoy tomado un enfoque distinto: construir un ambiente que les permita ser más productivo en este nuevo año, usando herramientas que puede que estén usando, o no.


## Mantener un diario con Jrnl {#mantener-un-diario-con-jrnl}

En mi trabajo, muches de nosotres posteamos un estatus de "fin del día" a Slack antes de terminar la jornada. Con un montón de proyectos en marcha y un equipo global, esta es una buena forma de compartir lo que hicimos, lo que está terminado, y con qué necesitamos ayuda. Pero algunos días son tan atareados, tan agitados que no puedo recordad qué es lo que hice. Y aquí es cuando el diario aparece.

{{< figure src="/ox-hugo/ejemploDeJrnl.png" >}}

En bastante fácil mantener un editor de texto abierto y tan solo agregar una línea cunado haces algo. Pero puede ser difícil ir para atrás y ver cuándo hicimos esa nota en particular o extraer anotaciones relacionadas de forma rápida y sencilla. afortunadamente, [Jrnl](https://jrnl.sh/) está acá para ayudarnos.

Jrnl nos permite tomar notas rápidas desde la línea de comandos, buscar entradas antiguas, y exportarlas a formatos de texto enriquecidos como HTML y Markdown. Podemos tener múltiples diarios, de forma que puedan tener las entradas de trabajo separadas de las entradas personales. Guardar estas entradas en texto plano, así que incluso si Jrnl dejara de funcionar, siempre tendremos nuestros datos de forma accesible.

Dado que Jrnl es un programa hecho en Python, la forma más fácil de instalarlo es con **pip install jrnl**; de esta forma nos aseguraremos de tener la última y mejor versión. La primera vez que lo corramos, nos hará algunas preguntas, y luego ya está todo listo para usarlo.

{{< figure src="/ox-hugo/configuracionDeJrnl.png" >}}

Ahora, cuando queramos crear una nota o registrar alguna tarea, simplemente tipearemos **jrnl <algo de texto>**, y será logueada con una marca temporal en el archivo por defecto. Podemos buscar entradas de alguna fecha específica con **jrnl -on AAAA-MM-DD**, entradas desde alguna fecha con **jrnl -from AAAA-MM-DD**, y entradas hasta alguna fecha específica con **jrnl -to AAAA-MM-DD**. Los términos de búsqueda pueden combinarse con el parámetro **-and**, permitiéndonos búsquedas como **jrnl -from 2019-01-01 -and -to 2019-12-31**.

También podemos editar las entradas del diario con esta línea de comando **--edit**. Antes de hacerlo, configuren el editor por defecto para las entradas editando el archivo **~/.config/jrnl/jrnl.yml**. En este lugar también podemos especificar qué archivos usar en los diarios, que carácter especial usar para el etiquetado, y algunas cosas más. Por ahora, lo importante es el editor. Yo uso Vim (yo, el traductor, Emacs ^\_^) y la documentación de Jrnl tiene algunas[ayudas muy útiles](https://jrnl.sh/recipes/#external-editors) para usar otros editores como VsCode y Sublime Text.

{{< figure src="/ox-hugo/jrnlConfig.png" >}}

Jrnl también puede encriptar los archivos del diario. Al configurar la variable global **encrypt**, le estamos diciendo a Jrnl que encripte todos los diarios que tengamos definidos. La encripción también puede ser configurada por archivos agregando **encrypt:true** en el archivo de configuración.

{{< figure src="/ox-hugo/JrnlConfigEncrypt.png" >}}

Si un diario no está encriptado aún, les pedirá una contraseña en cada acción para ese diario. El archivo del diario será guardado encriptado en el disco asegurándolo de miradas indiscretas. [La documentación de Jrnl](https://jrnl.sh/encryption/) tiene un montón más de información sobre como funciona, que cifrado usa y demás.

{{< figure src="/ox-hugo/jrnlPrivado.png" >}}

Mantener un diario me ayuda a recordar qué, cuándo hice algo y luego enontrarlo cuando lo necesito.
