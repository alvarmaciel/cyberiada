+++
title = "Organizando el trabajo con Orgmode para educadores"
lastmod = 2020-03-20T07:55:26-03:00
tags = ["orgmode"]
categories = ["nerdeadas"]
draft = true
image = "https://media.giphy.com/media/l2QZUJ0IubBLIf8A0/source.gif"
subtitle = "Como hago para organizar 3 roles y trabajos diferentes como educador"
+++

Esta es una recopilación de artículos, configuraciones y metodologías que uso para organizar las tareas que tengo como docente integrador, docente remoto de pensamiento computacional y referente redagógico del Programa de Pensamiento Computacional de la Fundación Sadosky para el Plan Ceibal.

<!--more-->

Hace años que vengo usando [Org-mode](https://orgmode.org) para gestionar mis tareas y mis escritos, de hecho, este mismo blog está escrito en org-mode. Org-mode es una suerte de complemento (Modo Mayor para ser precisos) de [Emacs](https://www.gnu.org/software/emacs/). Emacs es un editor de texto que tiene la ventaja de ser, ampliable, configurable y en general todos los archivos se producen en texto plano, el formato de texto más sencillo y sustentable que tenemos. Una de sus tantas ventajas es que nos permite escribir sin preocuparnos por el formato del texto y luego exportarlo al tipo de documento que querramos: pdf, doc, html y otros más.
Tiene una curva de aprendizaje elevada. Pero que vale la pena aprender.

{{< figure src="/ox-hugo/orgmodeyweb.png" caption="Figure 1: Ejemplo de uso de emacs + orgmode para este blog:  Así escribo | así se ve en la web" >}}

{{< figure src="/ox-hugo/escribiendoEnEmacs.gif" caption="Figure 2: Así escribo, pueden tener muchos themes claros, oscuros o editar el de ustedes" >}}


## Flujo de trabajo {#flujo-de-trabajo}

Básicamente lo que hago es concentrar en un solo archivo las tareas pendientes, las notas, los links y todo aquello que se me viene a la mente. Esto me libera. Me permite concentrarme en una cosa a la vez y mantener vinculado y funciona como un reservorio de todo ese flujo de cosas que me asaltan cada vez que hago algo. Es como tener un solo cuaderno para todo. Y ese cuaderno digital es un archivo que funciona como bandeja de entrada o inbox en mi caso mi archivo se llama  `inbox.org`

> Tu cerebro es para tener ideas, no para guardarlas - David Allen

esta frase esta tomada de [un post de Jethro](https://blog.jethro.dev/posts/capturing%5Finbox/) fuente muchas de las cosas que veremos en este artículo. `inbox.org` es donde todo comienza. todo artículo, pensamiento o tarea empieza en el inbox. y capturar esto en él debe ser lo más sencillo posible. Debemos confiar y entregarnos a que, todo sobre lo que tengamos que pensar, esté en el inbox. Esto nos permite concentrarnos al 100% en la tarea en la que estemos.

Las tareas se nos apilaran rápidamente y seguro que aparezcan mientras estamos en medio de algo. El inbox está configurado de forma que pueda capturar rápidamente lo que se me cruza en la cabeza y pueda seguir con lo que estaba haciendo.


### Registrar Tareas {#registrar-tareas}

Org-mode viene con algo llamado [org-capture](https://orgmode.org/manual/Capture.html), que nos permite guardar notas desde cualquier lado. Lo uso para agregar de forma rápida cualquier cosa a mi inbox.

![](/ox-hugo/capturandoConOrgmode.gif)
Esta captura luego se verá reflejada en mi agenda. Lugar desde donde el cual chequeo y reorganizo mis tareas.

{{< figure src="/ox-hugo/vistaDeAgenda.png" caption="Figure 4: Vista de mi agenda" >}}


### Notas {#notas}


### Calendario {#calendario}


### Celular {#celular}


## Configuración básica {#configuración-básica}


## Fin {#fin}

Imagen: [Giphy](https://gph.is/1UnMncr)
