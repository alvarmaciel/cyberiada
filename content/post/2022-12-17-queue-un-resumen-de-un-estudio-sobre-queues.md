+++
title = "Queues: Un resumen de un estudio sobre Queues"
lastmod = 2022-12-17T13:22:56-03:00
categories = ["nerdeadas"]
draft = false
image = "https://media.tenor.com/JI70JwoTcywAAAAS/pokemon-falinks.gif"
+++

El tema de las colas es algo a lo que no le pude dedicar mucho estudio. Llego el momento.

<!--more-->

![](/images/pokemon-falinks.gif)
Este es un resumen y una estrategia de estudio. A partir de [este articulo](https://realpython.com/queue-in-python/) en [Real Python](https://realpython.com), hago la toma de notas en castellano y escribo hablando lo más en criollo que puedo. Parte de la estrategia es también codear en castellano.


## Tipos de Queues(Colas) {#tipos-de-queues--colas}

Queue
: Es un [tipo de dato abstracto](https://en.wikipedia.org/wiki/Abstract_data_type) que representa una secuencia de elementos ordenados de acuerdo a un serie de reglas. Las colas proveen operaciones para agregar o remover elementos en tiempos constantes. Esto quiere decir que debe ser instantaneo, sin importar el tamaño de la cola.

FIFO
: First in First Out: La cola del banco

LIFO
: Last In First Out Aka Stack una pila que deja salir primero los ultimos elementos: La pila de ropa para planchar

Deque
: Double-end Queue o Deck. Una cola que permite sacar por derecha o izquierda y podés hacer rotaciones

Priority Queue
: Una cola con prioridades que se organizan jerarquicamente.


## Representar FIFO y LIFO coas con Deque {#representar-fifo-y-lifo-coas-con-deque}

Usaremos la `collection` [deque](https://realpython.com/python-deque/) de la standar lib, ya que para crear una cola FIFO o LIFO neceitamos que se cumpla ua secuencia [O(1)](https://realpython.com/binary-search-python/#the-big-o-notation), o de tiempo constante para la performance de encolado y desencolado. Y las double-ended queue nos habilitan esta propiedad.
Usamos `collection` en ligar de una `list` porque, si bien ambos elementos tienen el método `.append()`, para encolar elementos, desafortunadamente, desencolar un elemento de el frente de una lista (desde el indice 0) con `list.pop(0)` o insertarlo por el frente con `list.insert(0, elemento)`, es mucho menos eficiente. Estas operaciones siempre hace un shift del elemente remanente, dando como resultado una complejidad linear o 0(n) a diferencia de  `deque.popleft()` y `deque.appendleft()` evitan en ambos casos el shift de elementos.


## Construir una Queue Data type {#construir-una-queue-data-type}

Para hacer nuestra cola FIFO, necesitamos que esta pueda encolar y desencolar elementos. Vamos a crear ua clase que delegará estas dos operaciones usando instancia `collections.deque` llamada `._elements`

Peero antes un test, porque se, porque asi soy

Que tendría que tener nuestro test:

-   Dado un objeto tipo Cola con un método encolar y uno desencolar
-   Cuando llamamos a encolar el texto "primer elemento"
-   Y llamamos a encolar el texto "segundo elemento"
-   Y llamamos a encolar el texto "tercer elemento"
-   Entonces `_elementos` tiene 2 elements
-   Cuando llamamos desencolar
-   Entonces `_elementos` tiene 2 elements

Version 1 del test

```python

def test_cola():
    # Given
    fifo = Cola()
    # When
    fifo.encolar("1er elemento")
    fifo.encolar("2do elemento")
    fifo.encolar("3er elemento")

    #Then is a deque of three elemts
    assert len(fifo._elementos) == 3

    # When dequeue 1 element
    fifo.desencolar()

    #Then is a deque of three elemts
    assert len(fifo._elementos) == 2
```

La instancia de clase `._elementos` no debería ser llamada y accedida desde afuera por fuera de la clase, peeero se puede hacer. Para evitar esto vamos a implementar un dunder method `__len__` que nos devuelva el `__len__` de la `._elementos`
Asi que vamos con una versión 2 del test.

```python
from queues import Cola
def test_Cola():
    # Given
    fifo = Cola()
    # When
    fifo.enqueue("1er elemento")
    fifo.enqueue("2do elemento")
    fifo.enqueue("3er elemento")

    # Then is a deque of three elemts
    assert len(fifo) == 3

    # When dequeue 1 element
    fifo.dequeue()

    # Then
    assert len(fifo) == 2
```

Ahora si, con nuestro test fallado, vamos a codear la cola

```python
from collections import deque

class Cola:
    def __init__(self):
        self._elementos = deque()

    def __len__(self):
        return len(self._elementos)

    def enqueue(self, elemento):
        self._elementos.append(elemento)

    def dequeue(self):
        return self._elementos.popleft()
```

Solo para les que estén preguntandose como viene quedando todo en mi directorio de trabajo:

```shell
└── colas
    ├── __init.py__
    ├── colas.py
    └── tests
        ├── __init__.py
        └── test_colas.py
```

Podemos hacer la cola itereable, esto es que podamos usar un loop for y que le podamos pasar mas de un un argumento.
para pasar más de un argumento implementamos `*elementos` en el método inicializador. Implentando el dunder método `__iter__()` hacemos la clase iterable. Esta implementación es un ejemplo de un [generator iterator](https://docs.python.org/3/glossary.html#term-generator-iterator), que produce ([yield](https://realpython.com/introduction-to-python-generators/)) elementos de forma vaga ([lazy](https://en.wikipedia.org/wiki/Lazy_evaluation)). Lo que hace esta implmentación es también desencolar los elementos a medida que se itera.

Agregamos entnces un test:

-   Dado un objeto tip Cola en le que encolamos 3 elementos en el mommento de instanciarla
-   Cuando iteramos por el objeto
-   Entonces nos quedan 0 elementos

<!--listend-->

```python
def test_cola_es_iterable():
    # Given
    fifo = Cola("primero", "segundo", "tercero")
    assert len(fifo) == 3

    # When
    for elemento in fifo:
        elemento

    # Then
    assert len(fifo) == 0
```
