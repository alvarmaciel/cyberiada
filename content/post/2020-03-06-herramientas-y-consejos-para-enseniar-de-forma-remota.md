+++
title = "Herramientas y consejos para enseñar de forma remota"
lastmod = 2020-03-06T16:22:03-03:00
categories = ["educación"]
draft = false
image = "https://media.giphy.com/media/UHDRSmoQOQrPW/source.gif"
subtitle = "Otra traducción irrespetuosa. Esta ves de freetech4teachers"
+++

Este articulo es una traducción y adaptación del que fue publicado en [FreeTech4Teachers](https://www.freetech4teachers.com/2020/03/tips-and-tools-for-teaching-remotely.html) Aparentemente en USA están preparandose para dar clases remotas en caso de que cierren las escuelas por el [COVID-19](https://es.wikipedia.org/wiki/Epidemia%5Fde%5Fneumon%C3%ADa%5Fpor%5Fcoronavirus%5Fde%5F2019-2020) (más conocido como Coronavirus).

<!--more-->


## Tips para dar clases online {#tips-para-dar-clases-online}

-   Si van a dar clases usando servicios como los de Google Hangout Meet, Microsoft Teams, Zoom u otro servicio de clases en vivo, **mantengan las cámaras prendidas todo el tiempo posible**. parece una obviedad, pero no lo es. Les estudiantes pueden caminar, alejarse de sus computadoras mientras damos la clase, pero es menos probable que se aburran si pueden al menos ver sus rostros en lugar de tan solo una pantalla.
-   Eleven la webcam al nivel de los ojos o un poco más arriba. nadie, y en especial adolescentes, quieren estar viendo nuestras narices por media hora. Miren la webcam en lugar de las pantallas cuando quieran enfatizar algún tema. Tony Vincent da unos buenos ejemplos de como hacer esto en la presentación para la [Practical Ed Tech Creativity Conference](https://youtu.be/hl5WBoSmnQ8).
-   Condimenten la sesión online con muchas pequeñas preguntas de chequeo para el grupo. En una clase online no tenemos el beneficio de "captar el clima del aula" como lo hacemos en persona. Las preguntas de chequeo pueden ser tan simples como ¿Quién me está siguiendo? o más complicadas como ¿Cuál será la resolución del último problema?
-   Alienten a les estudiantes a hacer preguntas. Pueden proponer cosas como "dentro de 10 minutos vamos a hacer una pausa para que todes puedan hacer preguntas en el chat"
-   Esperen problemas técnicos en las clases en vivo. el 90% de los casos el problema técnico está en el lugar de los espectadores y no en nuestro lugar. Si pueden, configuren 2 computadoras o trabajen con un colega que esté desde la perspectiva de los estudiantes antes de salir en vivo con sus clases. Ver la perspectiva de los estudiantes hará que se aun poco más fácil proporcionar algunos consejos para solucionar los problemas que surjan en la marcha.
-   Si viven en una área rural, como es mi caso, y no tienen una conexión a internet superveloz, asegúrense de que el resto de las personas en sus casas no estén stremeando o, como en mi caso, usando la red ^\_^


## Tips para dar clases grabadas {#tips-para-dar-clases-grabadas}

-   Como en los casos de clases en vivo, traten de mantener sus rostros alejados de la cámara en el video. Herramientas como [Open Brodcaster Software](https://obsproject.com/) nos permiten grabar el video y que nuestro rostro aparezca en un pequeño recuadro en alguna de las esquinas.
-   "Lo bueno breve dos veces bueno". Es más probable que los estudiantes vean de corrido un video que dure dos a cinco minutos que uno de diez minutos. Excepto que se sientan muy a gusto en cámara y sean realmente buenos en enganchar a la gente a través de la cámara.
-   Mantengan activa al pantalla. Una forma simple de crear video lecciones es grabar un screencast de las diapositivas. También es una forma sencilla de matar de aburrimiento a les estudiantes si no se mantiene la pantalla con actividad. Agreguen maś transiciones y animaciones de las que usan usualmente en sus slides. Pueden ver este hermoso [tutorial de diapositivas interactivas con Inkspace + JessyInk](https://twitter.com/DaFeBa%5Ffoto/status/1158435313674596353?s=09)s. Aunque,si les parece mucho trabajo, entonces pueden probar el enfoque de Tom Richey. Tom divide la pantalla entre él y los slides, de esta forma siempre está ocurriendo algo en la pantalla.


## Herramientas para crear clases grabadas. {#herramientas-para-crear-clases-grabadas-dot}

-   Probablemente la forma más simple de grabar una video classe sea poner el teléfono en un trípode y grabarse a si mismo en frente de un pizarrón. Al finalizar la grabación, pueden usar la aplicación de yputube para hacer ediciones rápidas y luego publicarlo a sus estudiantes.
-   Si tienen cuenta de Youtube pueden hacer un vivo, que se grabará para que los estudiantes lo puedan ver. [Vean este video](https://youtu.be/efIJHM1hjRk) que muestra como es el proceso. Es importante que sepan que sus cuentas tienen que pasar por un proceso de verificación de 24 horas antes de hacer un vivo. [Este es un demo](https://youtu.be/6wluJrFL6bo) del autor original de este artículo.

El artículo original recomienda una serie de herramientas. Dado que no analicé sus políticas de privacidad y viendo que están lejos de mi concepción de herramienta abiertas y libres no las voy a linkear (con youtube no puedo hacer nada, me ganó hace años). Pero pueden recorrerlas y probarlas. Después me cuentan como andan.

Fuente de la imagen de cabecera: [Gyphy](https://gph.is/15FpGpC)
