+++
title = "Planificando la Cacería de Dragones"
lastmod = 2019-04-25T20:29:40-03:00
tags = ["juegos"]
categories = ["traducciones"]
draft = false
image = "images/dungeonworld.jpg"
+++

Esta es una traducción irrespetuosa de un documento que sirve mucho para hacer sesiones cortas y autoconclusivas (One Shot) de partidas de Dungeon World. Está muy bien organizado y detallado para todes aquelles que quieran empezar a dirigir juegos va muy bien

<!--more-->

<div class="ox-hugo-toc toc">
<div></div>

<div class="heading">Table of Contents</div>

- [INTRODUCCIÓN](#introducción)
- [CRONOGRAMA DEL JUEGO](#cronograma-del-juego)
- [INTRODUCCIÓN A DUNGEON WORLD](#introducción-a-dungeon-world)
- [CREACIÓN DE PERSONAJES](#creación-de-personajes)
- [HACIENDO EL MAPA](#haciendo-el-mapa)
- [P&R (Preguntas y Respuestas)](#p-and-r--preguntas-y-respuestas)
  - [PRINCIPIOS GENERALES DE LAS P&R](#principios-generales-de-las-p-and-r)
  - [PREGUNTAS PREMISAS](#preguntas-premisas)
  - [PREGUNTAS DE OPOSICIÓN](#preguntas-de-oposición)
  - [¡MÁS PREGUNTAS!](#más-preguntas)
- [VÍNCULOS](#vinculos)
  - [UN MOVIMIENTO PARA ESTABLECER MÁS VÍNCULOS DURANTE EL JUEGO](#un-movimiento-para-establecer-más-vínculos-durante-el-juego)
- [ESCENA UNO: PONIENDOSE EN MARCHA.](#escena-uno-poniendose-en-marcha-dot)
  - [LOS MONSTRUOS](#los-monstruos)
  - [ENFATIZAR MOVIMIENTOS DE BUSQUEDA DE INFORMACIÓN](#enfatizar-movimientos-de-busqueda-de-información)
- [ESCENA DOS: LA RAMIFICACIÓN](#escena-dos-la-ramificación)
- [INTERLUDIO: SUBIR DE NIVEL... IR AL BAÑO](#interludio-subir-de-nivel-dot-dot-dot-ir-al-baño)
- [ESCENA TRES: EL FINAL](#escena-tres-el-final)
  - [Hydra-Quimera](#hydra-quimera)
- [EPÍLOGO](#epílogo)
- [APÉNDICES: VARIAS OBSERVACIONES SOBRE DUNGEON WORLD](#apendices-varias-observaciones-sobre-dungeon-world)
  - [APÉNDICE A: CONSEJO SOBRE DESAFIAR EL PELIGRO](#appendice-a-consejo-sobre-desafiar-el-peligro)
  - [APÉNDICE B: MI PREGUNTA FAVORITA DE TODOS LOS TIEMPOS](#apendice-b-mi-pregunta-favorita-de-todos-los-tiempos)
  - [APÉNDICE C: LOS PUNTOS DE GOLPE SON LA MUERTE](#apendice-c-los-puntos-de-golpe-son-la-muerte)
  - [APÉNDICE D: TEMAS COMUNES EN LA CREACIÓN DE PERSONAJES:](#apendice-d-temas-comunes-en-la-creación-de-personajes)
- [GRACIAS A:](#gracias-a)
- [OTRAS LECTURAS](#otras-lecturas)
- [OTRAS CONVERSACIONES](#otras-conversaciones)
- [HISTORIAL DE REVISIONES](#historial-de-revisiones)

</div>
<!--endtoc-->

- [Enlace al documento original](https://docs.google.com/document/d/17ypjtlHfcwqrU%5F-x4b7o0e8tZ%5FdN2TiNLUu48MLAw7Y/edit?hl=en&forcehl=1#heading=h.kce6i5lodbq1)
- [Página de John Aegard](http://john.aegard.com/)
- [ENLACE A LA VERSIÓN PDF, EPUB, MOBI](https://www.gitbook.com/book/alvarmaciel/armando-partidas-rapidas-de-dungeon-world/)

## INTRODUCCIÓN {#introducción}

Mi conocimiento literario está basado en relatos del genero de ficción, por eso cuando dirijo una partida auto-conclusiva (one-shot)de [Dungeon World](https://github.com/Sagelt/Dungeon-World), quiero que el juego parezca una sencilla y tradicional historia de aventuras fantásticas --una historia en la cual algunos personajes viajen a alguna parte, hagan algo, y en la que todos cambien por el viaje.

Tampoco tengo ganas de hacer ninguna preparación previa. No quiero aparecer con una ambientación o un escenario o una situación. Por un lado, eso implica trabajo, y soy un haragán. Por otro lado, no quiero estar contando mi historia, quiero estar para ayudar a contar nuestra historia. Quiero que todos contribuyan con ideas. No me gusta que los jugadores tengan que meter a presión sus contribuciones a través de la estrecha apertura de sus personajes. Quiero que todas las cosas importantes de nuestro juego pasen ahí en la mesa, no en mi cabeza de antemano.

He desarrollado algunas técnicas que me ayudan a lograr este objetivo de forma bastante consistente. Si quieren dirigir partidas auto-conclusivas ajustadas de Dungeon World, ¡esto puede servirles de ayuda!

En mi experiencia, estas técnicas sirven para un número máximo de cinco personajes jugadores. Con cinco, el tiempo de atención es muy justo, especialmente en las peleas. Creo que el número óptimo es de 3-4 personajes jugadores.

## CRONOGRAMA DEL JUEGO {#cronograma-del-juego}

Estos serán los objetivos a alcanzar en estas 4 horas:

- 0:00: [Introducción](#introducción-a-dungeon-world). [Charla sobre seguridad](/img/CruzadaManija.jpg). Juegos de Rol y demás. Comenzar a crear los personajes y hacer P&R.
- 0:30: P&R completa. Personajes, ambientación y set de premisas. Arranca el primer encuentro.
- 2:15: 1ra y 2da escena terminada; 15 minutos para subir nivel y para hacer un recreito
- 2:30: Comienza la segunda escena.
- 3:45: Escena final terminada, epílogo con los jugadores, agradecer por jugar.

## INTRODUCCIÓN A DUNGEON WORLD {#introducción-a-dungeon-world}

Así es como introduzco el juego y mi filosofía:

_"Dungeon World es un juego de aventura fantástica. Jugarán como una aventurera en un mundo peligroso. Pueden pelear por la gloria, por ganancias, por el bien o el mal o por lealtad a sus amigos. Eso lo decidirán aquí en la mesa, y la aventura emergerá de sus decisiones."_

_"No jugaremos los orígenes de la historia de este grupo. Jugaremos a través de una de sus aventuras, así que empezaremos como si nos conociéramos todas y estuviéramos viajando hacia el mismo lugar. Vamos a resolver la dinámica de las relaciones y los detalles específicos de nuestro viaje cuando creemos nuestros personajes"_

## CREACIÓN DE PERSONAJES {#creación-de-personajes}

Las jugadoras eligen sus personajes de sus libros de jugadoras o copias que les llevemos, como siempre, pero dejando los **Vínculos** para más tarde. **No hagan una vuelta de presentación de los personajes**. Conoceremos sobre los personajes a medida que sepamos sus relaciones con la historia. Chequeen la lista de confusiones comunes en la creación de personajes en Dungeon World en el [APÉNDICE D: TEMAS COMUNES EN LA CREACIÓN DE PERSONAJES:](#apendice-d-temas-comunes-en-la-creación-de-personajes) .

## HACIENDO EL MAPA {#haciendo-el-mapa}

\![map](/assets/img/map.jpg)

Me gusta tener un mapa. No lo uso para nada formal, pero es una buena base para la ambientación y una chance para garabatear un poco.

El mapa será una red de papeles o tarjetas puestas de manera que todas puedan verlas. A medida que el juego avance, se mencionarán los lugares. Capten cada uno de ellos en una tarjeta y agrénlo al mapa. También, a medida que se agreguen premisas y PNJ's, asegúrense de marcar dónde estas viven. Agreguen tarjetas que las ubiquen en esos lugares.

Un mapa hecho de tarjetas es súper flexible y les ayudara a ganarse totalmente la medalla de **Dibujo mapas mientras dejo espacios en blanco**. Nótese, que si quieren agregar una locación entre otras dos locaciones en medio del juego, tan solo insertan una tarjeta entre estas últimas. En la foto del mapa, nosotras descubrimos la zona "Burn Zone" a medio camino en la partida, así que tan solo la insertamos y listo.

## P&R (Preguntas y Respuestas) {#p-and-r--preguntas-y-respuestas}

Este es el paso pre-juego más importante; acá se genera la ambientación, premisas y vínculos, donde nosotras uniremos a cinco extrañas en la locura del juego de rol.

### PRINCIPIOS GENERALES DE LAS P&R {#principios-generales-de-las-p-and-r}

Primero, un punto súper importante: \***\*Todas las preguntas serán dirigidas a una jugadora específica, no a la mesa como un todo\*\***. Preguntar a la mesa algún tema solo invita a que las personas habladoras contribuyan más. ¡Nosotras queremos que nuestras P&R sean equitativas! Asegúrense de que las preguntas se distribuyan uniformemente entre todas en la mesa.

El DJ dirige las P&R y hace las preguntas, pero cualquiera es bienvenida a hacer sus propias preguntas si quieren.

Algunas clases pueden elegir cosas de una lista en la creación de personajes -- el Guerrero elije un arma, el Bardo elije una área de conocimiento, los magos sus hechizos, etc. ¡Es muy poderoso y estimulante cuando la historia refleja directamente estas elecciones!

Usen múltiples preguntas sobre un mismo tema para ir generando el tono en sus ambientaciones y para crear distintos -- tal vez contradictorios -- objetivos para los personajes. Piensen en \***\*Rashomon\*\***, sobre como distintas personas saben y ven las distintas partes como un todo.

Sigan haciendo preguntas hasta que las premisas sean claras y todos los personajes tengan una conexión con al menos otro de ellos.

Es totalmente legítimo hacer preguntas que vayan en un sentido que les permitan anclar algún elemento ficcional que quieran incluir en el juego. Por ejemplo, si quieren usar la Hydra-Quimera (se detalla más adelante) como monstruo jefe, pueden meterla en una pregunta: ¿Guerrero, por qué querés enfrentarte a la sanguinaria Hydra-Quimera?

#### PLANTEANDO PREGUNTAS {#planteando-preguntas}

Hagan algunas de estas preguntas a fin de construir una base para la ambientación. Me gusta dirigir estas preguntas a personajes que intrínsecamente sugieren una localización: El Druida, El Explorador o El Ladrón.

Estos son unos ejemplos:

> Druida, ¿Cómo es tu hermosa tierra natal ?

> Exploradora, ¿Qué tierras exploraste? ¿Cómo eran?

> Ladrona, ¿Cómo son tus territorios usuales?

> Cualquiera, ¿A que lugar has viajado?

Pueden preguntar a otros jugadores algunas de las siguientes preguntas si quieren mejorar la ambientación:

> A la Bardo, ¿Cuándo fue la última ves que estuviste aquí? ¿Cómo era el lugar entonces? ¿En qué ha cambiado?

> ¿Clériga, por qué las peregrinas de <nombre del Diosa> viajan hasta este lugar? ¿Por qué este lugar es tan especial para la Diosa <Nombre de la Diosa elegido por la clériga>

> Guerrera, ¿Qué batalla se combatió aquí? ¿Quienes combatieron?

> Ladrona, ¿Qué fue lo que robaste de acá en tu pasado?

> Maga, ¿Por qué este es un lugar de poder? Qué tipo de rituales se realizan aquí? ¿Qué ritual llevarías acabo acá?

### PREGUNTAS PREMISAS {#preguntas-premisas}

Por _premisa_, quiero decir **qué es lo que los personajes están haciendo y por qué**, La generación de premisas es la parte más crítica de esta metodología.

\***\*CUANTAS PREMISAS\*\***: En mi experiencia, tener múltiples y distintas premisas en un juego para cuatro, definitivamente funciona. Algunas veces las jugadoras pueden enredarse de formas extrañas en la escena final, lo que usualmente es muy satisfactorio. Está bien tener tan solo una sola premisa; hará el juego más sencillo.

\***\*DESTINO FINAL\*\***: Asegúrense de que todas las premisas apunten hacia un mismo lugar en el mapa. Ese será el lugar donde ocurrirá la escena final. Hablaré mucho sobre ese lugar, así que le voy a poner nombre y se llamará el **Destino Final**.

Cuando hagan la primer premisa, el lugar que asignan se convierte en el Destino Final. Si establecen otra premisa, asegúrense que referencia al Destino Final.

Estas son algunas preguntas básicas que establecerán las premisas:

> Paladina, ¿cual es tu misión (quest)?

> Ladrona, ¿Que es lo que viniste a robar?

> Maga, ¿Qué magia o misterio te trajo hasta acá?

> Clériga, ¿Qué profecía estás tratando que se cumplir o impedir acá?

> Druida, ¿Que perturba a estas tierras, y cómo pueden ayudar estos aventureros?

> Barbara, ¿Qué apetito te trajo aquí?¿Cómo lo vas a satisfacer?

> A la Bardo, ¿Qué leyenda te arrastró hasta acá? ¿Que esperas ver mientras estés acá?

> A cualquiera, ¿Por qué estás viajando a este lugar?

Una vez que establecieron las premisas, es tiempo de enredar al grupo, Usarán sus habilidades de DJ para armar triángulos entre dos personajes y la premisa, a la forma clásica de Apocalipsis World.

Por ejemplo, pueden usar triángulos personaje-premisa-personaje para meter tensión en sus jugadores. Por ejemplo

> <DJ> Paladina, ¿Cuál es tu misión?

> <Paladina> Estoy acá para prevenir que el Nigromante complete su conjuro.

> <DJ> Ladrona, ¿Qué necesitas robar del Nigromante?

> <DJ> Maga, ¿Todavía sos amigo del Nigromante? ¿Dónde fue que lo conociste?

> <DJ> Guerrera, ¿Cómo reconocerás quien es un siervo del Nigromante?

Si quieren un juego menos contencioso, pueden usar triángulos para hacer que los objetivos de los personajes se completen unos a otros. Por ejemplo:

> <DJ> Druida, ¿Qué perturba a estas tierras?

> <Druida> El dragón de pantano Poxtoxalis, que toma muchas presas de caza.

> <DJ> Guerrera. ¿Por qué querés matar a Poxtoxalis?

> <DJ> Ladrona, ¿Qué le robaste a Poxtoxalis?

> <DJ> Exploradora, ¿Qué pasó cuando trataste de cazar a Poxtoxalis?

\***\*MÁS ALLÁ DE LAS PUERTAS DE LA MUERTE YACEN LAS PREMISAS\*\***: El movimiento **Último Aliento**, es una característica única de Dungeon World y en mi experiencia los jugadores realmente responden a ella. Para hacerlo, necesitan un buen trato con la Muerte, así que vamos a mostrar uno acá.

Pónganse su gorra de DJ malo y consideren sus premisas. Piensen en lo que pasará si el personaje no interviene -- justo como si tuvieran que crear un Fatalidad Inminente para un frente de aventura. Si no se les ocurre ninguna idea, tan solo pregunten. "Paladina, ¿Por qué están tratando de apoderase del libro de Conjuros Superiores del archimago Doorval? ¿Por qué no podés permitir que lo tenga? ¿Qué es lo que hará con él?"

Cuando alguna logra eludir la muerte, que jueguen **último aliento** como siempre, Con un resultado de 7-9, la Muerte ofrece un trato: Si el personaje moribundo negocia, la Fatalidad Inminente se hace realidad, la Muerte lo dejará ir. Consideren tomar los resultado 2-6 como resultados 7-9. estos tratos siempre son divertidos.

También, mantengan esa Fatalidad Inminente en mente por todas las razones habituales -- como **cuando quieren mostrar pequeños señales de una amenaza o revelar una verdad incómoda**.

### PREGUNTAS DE OPOSICIÓN {#preguntas-de-oposición}

Hagan una o más de las siguientes preguntas para sacar ideas de oposición.

> A la Bardo o Maga, ¿Por qué necesitaron protección cuando viajaron hacia acá?

> Clériga o Paladina, ¿Qué enemigos de la fe moran por acá?

> Druida, ¿Qué perturba las tierras?

> Guerrera o Barbara, ¿Qué vinieron a matar? ¿Por qué lo odian o temen?

> Ladrona, ¿Quién controla este territorio?

> Exploradora, ¿Qué bestias cazan por estas zonas?

> A cualquiera, ¿Quién más en el mundo quiere conseguir <premisa>?

> A cualquiera, ¿Qué está protegiendo <premisa>?

### ¡MÁS PREGUNTAS! {#más-preguntas}

El brujo de Dungeon World Tim Franzke ha compilado [una maravillosa lista de consejos específicos para el DJ](https://docs.google.com/document/d/1f2MR7NyN41T9QxsYXUhMJCUvXWWX0VYxp1tAtYiOxWs/edit) que los ayudaran a ser unos fanáticos de los personajes. Mírenlos para tener más ideas sobe como tejer su juego de DW a su grupo de jugadoras.

## VINCULOS {#vinculos}

Una vez que tengan bastantes P&R para establecer el escenario, pidan a cada personaje que escriba un vínculo. Si necesitan alguna ayuda, acá hay algunas preguntas que pueden hacer y que apuntan a vínculos estándares:

> Barbara, ¿Quién de aquí casi te mata? ¿Cómo?

> Bardo, ¿Con quién de aquí has viajado antes?

> Clériga, ¿Quién, de los que están acá, crees que es una persona buena y fiel? ¿Por qué?

> Druida, ¿Con quién de acá has compartido un rito secreto de la tierra? ¿Por qué confías en el/ella?

> Guerrera, ¿A quién juraste proteger?

> Paladina, ¿Quién de aquí a estado con vos en batalla?

> Exploradora, ¿A quién has guiado antes?

> Ladrona, ¿Quién de aquí sabe cosas incriminantes sobre vos? <A la otra jugadora>, ¿qué es lo que sabes?

> Maga, ¿Quién de aquí, tendrá un papel relevante en los eventos que se avecinan?

Y acá hay algunas preguntas que pueden crear vínculos alternativos:

> Barbara, ¿Quién de aquí podría andar bien en tu tierra natal? ¿Cómo la invitarías a ir con vos?

> Bardo, ¿De quién has oído hablar antes? ¿Qué es lo que oíste?

> Clériga, ¿A quién te negaste a sanar? ¿Por qué?

> Maga, ¿Con quién estás experimentando? ¿Qué es lo que le hiciste?

### UN MOVIMIENTO PARA ESTABLECER MÁS VÍNCULOS DURANTE EL JUEGO {#un-movimiento-para-establecer-más-vínculos-durante-el-juego}

\***\*Cuando ayudas o interferís\*\***, tiren +vinculo como se hace normalmente. Si no tienen vínculos con la destinataria, tira +nada. Después de que tu beneficiario hace su movimiento, escriban un vínculo con él/ella inspirados en lo que haya ocurrido en el movimiento.

## ESCENA UNO: PONIENDOSE EN MARCHA. {#escena-uno-poniendose-en-marcha-dot}

Muy bien, tenemos los personajes, la ambientación, y algunas premisas. ¡Es momento de dados! Este es mi guión para arrancar con todo:

_"Dungeon World es una conversación entre todas nosotras. Ocasionalmente algo ocurre en la conversación que desencadena la reglas y a los dados. Al principio, yo voy a mencionar estos movimientos desencadenados por las reglas, pero en la medida que se familiaricen con las reglas, siéntanse libres de mencionarlas ustedes"_

Comiencen con el grupo en un lugar que estará alejado una o dos tarjetas (en el mapa) del destino final. Cuando se alce la cortina, Estarán frente a frente con algún monstruo. Comiencen en el instante en el que el grupo de encuentra con los monstruos. \***\*No arranquen con las hostilidades inmediatamente\*\***. Den a los jugadores una chance de cruzar la pelea con movimientos sociales o de conocimiento -- más a continuación.

Describan los alrededores y a los monstruos y pregunten, "¿Qué hacen?"

### LOS MONSTRUOS {#los-monstruos}

Asegúrense de saber porqué los monstruos están ahí --debe ser relevante a algo que haya sido revelado en las P&R. Pueden ser peones del Jefe Final, enviados a pelear con el grupo, o tal vez, están tras los mismos objetivos que el grupo.

Si las jugadoras han contribuido con ideas para la oposición, entonces usen definitivamente esas ideas para crear monstruos.

Mi práctica es poner a la gente en el primer encuentro con una turba de 6-10 medio amenazantes pequeños seres, **4PG**, **0 Armadura**, **d6 de daño**, con un movimiento de ataque que los haga un poco interesantes y un movimiento de movilidad que prevenga que el campo de batalla se convierta en algo muy estático.

Mi monstruo de introducción es algo con un movimiento que atrape -- una red, un látigo, una lengua prensible, etc. He usado a sapos cultistas con largas lenguas roba cosas, arañas cultistas con lanza redes, Anubitas cabeza de chacal con látigos. Usualmente le doy a alguno de ellos alas, saltos largos, enterramiento o nado.

\***\*No usen los viejos, aburridos y familiares monstruos, como orcos, goblins, kobolds, etc, para el primer encuentro\*\***. Queremos que el grupo tenga curiosidad sobre las cosas con las que están pelando, que están preguntando y disparando movimientos de busqueda de información, como **exhibir conocimientos, discernir la realidad y conocimiento de la Bardo**

### ENFATIZAR MOVIMIENTOS DE BUSQUEDA DE INFORMACIÓN {#enfatizar-movimientos-de-busqueda-de-información}

Queremos que las jugadoras estén realizando estos movimientos, porque son una expresión de curiosidad, y cada una demuestra enganche, y queremos recompensar y responder a ese enganche. Cada movimiento de conocimiento es una chance para las personas en la mesa de revelar la trama o la ambientación. Además --¡Importante!-- los movimientos de información o conocimiento pueden ser usados para cortocircuitar encuentros y estos son enemigos súper poderosos de la picadora de puntos de golpe [vean el apendice C para una mejor explicación](#apendice-c-los-puntos-de-golpe-son-la-muerte).

Al principio, busquen una oportunidad para mostrar su poder. Hagan que el resultado de un movimiento de información sea obviamente útil incluso con un 7-9. Usen sus respuestas para revelar tácticas decisivas para el encuentro. Ejemplos de mis últimos juegos: 1) El símbolo sagrado de la paladín es lo que está provocando a los ghouls del lago y si el grupo la arroja al agua, los no muertos se zambullirán atrás de él. 2) El pueblo-chacal de desierto obedece a su Alpha. Derrotar al Alpha es la forma de convertirse en un nuevo Alpha.

Den una importancia similar a los movimientos sociales como **parlamentar**. Sean estrictos con el grupo para hacer que el grupo demuestre su ventaja, pero no teman dejar que una posible ventaja y una buena roleada decidan un encuentro.

También, si se van a los espadazos, llamen la atención en el hecho de que Saja-Raja (Hack and Slash) y disparar no especifican exactamente que una tirada solo puede pegar a un objetivo. Las Barbaras y Guerreras deben estar saltando en la refriega y disfrutando con estos pelagatos. Los exploradores deben estar moviéndose entre ellos como heno fresco.

## ESCENA DOS: LA RAMIFICACIÓN {#escena-dos-la-ramificación}

Ok, terminó el primer encuentro, Ahora estamos listas para ver como el grupo hará para llegar a su destino final. Pongan en columna tres tarjetas de ubicación entre el lugar en el que están y su destino final. Dejen que estos lugares sean inspirados por cualquier cosa que haya salido en las P&R - la exploradora por ahí mencionó que viajó por algunas montañas, o la ladrona tal vez comentó algo sobre un pasaje secreto a través de los pantanos.

Diganles que pueden viajar al destino final por alguna de esos nuevos lugares.

Una vez que hayan elegido, golpeen con otro encuentro presentando un monstruo inusual esto es adecuado para demostrar el poder del lugar ficticio en el que están. Mi favorito para usar acá son los monstruos que petrifican, como el basilisco o la medusa. Estos son enemigos famosos que no se los combate de forma sencilla, se necesita ser más hábil o planear el combate para engañarlos.

También, la petrificación parcial es algo divertido de hacer a algún personaje; suelo comenzar con ojos, nariz y pelo. Es incluso más divertido cuando tienen a una auto sacrificada Paladín en el grupo que está ansiosa de mostrar su rectitud moral con una imposición de manos, o una bruja que quiere improvisar un ritual anti petrificación.

Pueden hacer otras cosas en la escena 2. Consideren golpearlas con algún movimiento de Localización apropiado para el camino que eligieron -- que se pierdan en el bosque, exponganlas a los elementos en el desierto, y demás. También pueden \***\*mostrar una oportunidad\*\*** para darles alguna aliada o alguna ayuda para la batalla final -- Por ahí un campamento rebelde lleno de soldadas que puedan reclutar, un mago itinerante que sabe cosas útiles, una secuaz descontenta con algún mapa, contraseña, etc.

## INTERLUDIO: SUBIR DE NIVEL... IR AL BAÑO {#interludio-subir-de-nivel-dot-dot-dot-ir-al-baño}

Subir de nivel es divertido y sencillo en DW. Algunos de los movimientos avanzados son geniales -- chequeen Maestría Elemental de la Druida, Altamente Lógico de la Maga y Tutela Divina, de la Paladín. Altamente Lógico convierte a la Maga en una genia táctica. Tutela Divina hace a la Paladín indestructible. Maestría Elemental es de los poderes más grosos que encontré en el libro.

La mayoría de las jugadoras pueden subir de nivel en 10 minutos más o menos -- por ahí un poco más para las conjuradoras o personas que hacen movimientos multiclase.

## ESCENA TRES: EL FINAL {#escena-tres-el-final}

Ok,llegamos, ¡este es el gran final! Generalmente les tiro un Monstruo Jefe bien grande al grupo, uno que esté inspirado en las P&R.

Mi monstruo jefe final favorito hasta ahora es algo que llamo Hydra-Quimera, una bestia regenerativa de seis cabezas. Me gusta porque es un monstruo épico único y grande que pelea como seis monstruos y tiene mucha superficie para interactuar con él: Pueden estar encima de ella, debajo, sobre alguno de sus seis cuellos, frente a alguna de las seis cabezas, atrapada en alguna de sus seis mandíbulas, etc. Y recompensan la osadía: sus cabezas son formidables, pero su cuerpo es blando.

### Hydra-Quimera {#hydra-quimera}

#### Gran construcción mágica {#gran-construcción-mágica}

- \***\*Cabezas de cabra (2)\*\***: daño m[2d8], 10 PG cada una, amadura 3, fuerte
  - Golpea a los voladores
- \***\*Cabezas de León (2)\*\***: daño m[2d12], 10 PG cada una, armadura 2 , desorganizada
  - Aturdir con el rugido
  - Sacudir y quebrar
- \***\*Cabezas de serpiente flamígera (2)\*\***: daño m[2d10], 10 PG cada una, armadura 2, medio alcance
  - Eructa un cono de fuego
  - Acierta perfectamente en la oscuridad
  - Inyecta veneno que hierve la sangre
- \***\*Cuerpo\*\***: m[2d8] damage, 16 PG, armadura 1
  - Regenera una cabeza
  - Pisotea todo bajo sus pies
  - Vuela torpemente

Sea cual sea el monstruo que usen, pongan un terreno interesante en el área del encuentro. La elevación siempre garpa, especialmente las elevaciones que no obstaculizan los ataques del monstruo jefe. incluyan agua para hundirse y fuego para encenderse. Un teatro/coliseo/tribunas llenas de espectadores es un buen agregado también; Nada se mete tanto bajo la piel de una gladiadora que las burlas de un borracho comerciante hombre-rana. Tal vez los espectadores puedan tirar tomates podridos y ladrillos, haciendo 1d4 de daño o daño cegador.

Incluso mientras la gran batalla se está librando, es probable que haya algo de las premisas que abordar -- cosas para robar, príncipes que rescatar, rituales que conducir, etc. Dirige los ojos de los personajes hacia ellas. Usen **mostrar oportunidades** para que piensen en lo que desean. Tientenlas para que pongan sus intereses sobre los del grupo

Jueguen lo suficientemente fuerte como para que vayan a visitar a La Muerte

Entonces, cuando la lucha acabé, el grupo tendrá que resolver los enredos de sus premisas. En mi experiencia, esta es la parte de mayor energía del juego, especialmente si hay contiendas. No es raro que haya PvP. Sientense y disfruten de lo que han forjado.

## EPÍLOGO {#epílogo}

Bien, es el fin del juego y ¡las cosas se resolvieron! Circulen por la mesa, terminando en el personaje al que más cosas dramáticas le hayan pasado. Pregunten a cada jugadora por turnos que narren un breve epílogo describiendo su vida a partir de la aventura. Me encantan los epílogos; son buenas oportunidades para que las jugadoras se suelten un poco solas, bajen sus defensas, y cuenten una historia con la que el DJ no puede meterse.

También tienen que describir epílogos para cualquier de sus PNJ's favoritos o localizaciones.

¡Eso es todo! Gracias a todos por jugar y si hay tiempo, tal vez pueden hacer un pequeño feedback para el próximo encuentro.

## APENDICES: VARIAS OBSERVACIONES SOBRE DUNGEON WORLD {#apendices-varias-observaciones-sobre-dungeon-world}

### APPENDICE A: CONSEJO SOBRE DESAFIAR EL PELIGRO {#appendice-a-consejo-sobre-desafiar-el-peligro}

El resultado 7-9 de Desafiar el Peligro puede ser muy difícil de decidir. Acá tienen una guía para hacer de estos resultados algo entretenido.

\***\*MIRA TUS MOVIMIENTOS\*\***: Para crear un peor resultado, miren a Desafiar así:

- +10: PJ desafía el peligro
- 7-9: PJ desafía el peligro y DJ hace un movimiento
- -6: DJ hace un movimiento

Supongamos que Amara, la ladrona está escapando del guardia de la ciudad y tira un 7-9 con el Peligro de ser atrapado. Como DJ, Le voy a dar a Amara su escape. Pero también puedo:

- Usar un movimiento de monstruo, peligro o locación -- Amara cae accidentalmente en un concilio de Necrodancers y es alcanzada por la maldición del movimiento descarado.
- Hacer daño -- Amara se tuerce el tobillo mientras corre.
- Bajarle los recursos -- Amara tira algo de su equipo mientras escapa.
- Ubicarla en algún lugar -- Amara escapa de la persecución... Pero cae en el pozo de un ogro.
- Separar el grupo -- ¡Omara se pierde!

\***\*LOS PEORES RESULTADOS PUEDEN SUAVIZARE CON HORRIBLES ELECCIONES\*\*** Pueden convertir una pifia en un trato difícil, explicando cual sería el resultado de la tirada y dejando que el jugador elija entre caer en el peligro que quería evitar o sufrir las consecuencias de la tirada. Creo que esto suaviza, de alguna manera el movimiento; estarían colaborando con el destino del jugador en lugar de imponérselo.

\***\*PONER A ALGUIEN MÁS EN LA MIRA.\*\*** ¿Hay algún otro personaje en una posición comprometida? ¡Puede recaer en él/ella el aspecto negativo del 7-9?

Supongamos que Omara saltó sobre un mamut peludo enloquecido y está tratando de calmarlo mientras Sanguinus La Paladín combate contra un cocodrilo cercano. En la resolución, Amara calma el mamut, pero no antes de que se haya caído sobre Sanguinus.

Digamos que Amara está arrastrándose por el distrito en cuarentena, robando en las casas de los muertos. El Peligro es que se agarre la plaga. Con un 7-9, Amara no se contagia... pero deviene un portador, y sus amigas del grupo se la pescan un tiempo después.

Ahora Amara está leyendo el Cántico de Sog-Yothot para tratar de cerrar la Puerta Asesina (peligro: que la puerta quede abierta) Mientras Xeno, la Maga lanza su hechizo Clones Espejo para distraer a algunos cultistas. La Puerta Asesina se está cerrando, pero un flujo de energía hace que una de los clones se convierta en permanente, independiente y con alineación inversa a la versión de Xeno.

A veces voy a trampear en esto e ir directamente hacia otro personaje justo antes de la tirada del movimiento Desafiar el Peligro así la situación puede desarrollar algunas facetas más antes de empezar a complicarnos con los éxitos parciales.

\***\*CONSIDEREN EL PELIGRO IMPLICADO EN TODAS LAS ESCALAS TEMPORALES\*\***: Usualmente categorizo el peligro como, peligro inmediato, escenas peligrosas o campañas peligrosas. Los agrupo preguntando cuando el dolor del peligro bajará. Si va a bajar enseguida, es un peligro inmediato. Si va a bajar hacia el final de la escena, es una escena peligrosa, Si bajará tiempo después, es un peligro de campaña..

Por ejemplo, digamos que Amara la Ladrona está por saltar hacia un techo (peligro inmediato) tratando de escapar de la guardia (escena peligrosa) mientras trata de permanecer anónimo (peligro de campaña). Estos son los tres peligros en una simple acción. Uno de estos debe ser declarado como Desafiar el Peligro.

Digamos que el Peligro es que puede caerse. Las complicaciones del éxito parcial puede ser hace el salto y escapa, pero es claramente identificado por sus perseguidores y pierde el anonimato. Otra posibilidad es que hace el salto pero sus perseguidores también, así que tiene que pensar alguna otra táctica para eludir a los perseguidores.

Alternativamente, digamos que el peligro es ser atrapado. Con un éxito parcial, por ahí elude a los perseguidores -- cayéndose y torciéndose el tobillo mientras los perseguidores se concentran en el techo. O puede escapar, pero perder su anonimato.

### APENDICE B: MI PREGUNTA FAVORITA DE TODOS LOS TIEMPOS {#apendice-b-mi-pregunta-favorita-de-todos-los-tiempos}

Algunas veces algún PJ está completamente enganchado. Va aterrizar en un lugar terrible, donde la muerte parece inevitable. Cuando esto ocurra ¡No lo mates!, en lugar de eso, hace Mi Pregunta Favorita de Todos los Tiempos

> "¿Cómo diablos sobrevives a ESTO?"

Cada vez que he usado esta pregunta, ha habido grandes éxitos. Para demostrarlo, esta es una de mis historias favoritas:

Un PJ elfo a armado una poderosa bomba mágica en el sótanos del la Torre de la Hechicería. Mientras explora la Torre, es acorralado por unos Guerreros del Sueño. Sin escapatoria posible, detona remotamente la bomba, ya habíamos establecido, que tiraría abajo definitivamente la torre.

"Okey, ¿Cómo diablos sobrevives a esto?" Pregunto.

"Reencarno", me dice

Ahora bien este Mago-Elfo era un vejo racista anti-humano. Y vi la oportunidad de imponer algún tipo de la antigua y bien ponderada ironía moralizante de La Dimensión Desconocida en su elfo trasero. "Entonces Desafía el peligro con +nada", le digo, "O te voy a hacer volver como un sucio humano"

Por supuesto, el toma la tirada. Se despierta en los brazos de una partera elfa... muchos siglos despues, aplausos en la mesa, incluso de aquellos que jugaban como humanos.

### APENDICE C: LOS PUNTOS DE GOLPE SON LA MUERTE {#apendice-c-los-puntos-de-golpe-son-la-muerte}

Debido a que Dungeon World es un juego que da la apariencia de tener tiradas de "golpe (Saja-Raja/Disparar)" y Puntos de Golpe, es muy sencillo caer en la viejo estilo de tictac golpear, errar, golpear, acertar, daño, golpear, errar, golpear, daño, el enemigo cae, etc. Mientras la ficción cae detrás. **ESTO ES UNA ABURRIDA; DENSA MUERTE.**

Si sienten que esto está pasando, transformen la situación ficcional con cada movimiento que hagan. Fuercen a los jugadores a maniobrar. Dibujen un mapa para tenerlos pensando sobre las oportunidades. Demuestren el poder supremo del posicionamiento ficcional.

Si alguna Guerrera o Paladín está solo machacando monstruos sin imaginación, denle en el rostro. Usen alguna oportunidad de oro disparada por cualquiera en la mesa para tirarles algún movimiento difícil. Noqueenlos, ensucienlos, ponganlos en lucha cuerpo a cuerpo, cuando sus lujosas espadas y lanzas sean inútiles. Desarmenlos con un movimiento de la lucha. Haganlos Desafiar el Peligro y hagan que ellos tengan que decir como Desafiarían el peligro.

Magas y Exploradoras son más susceptibles al juego aburrido. Con sus movimientos, pueden sentarse tranquilos y disparar a sus blancos desde la distancia. El resultado "Tenés que acercarte al peligro para poder acertar el tiro" de un 7-9 en disparar es buenísimo, si bien incompleto, cura este problema.

Tiren a estos artilleros al agua, hagan que las cuerdas de sus arcos o libros de hechizos se mojen, iluminenlos con fuego, manden monstruos voladores extra o saltadores sobre ellos. Sugieran que estarían mejor si la Guerrera pudiera defenderlas. El movimiento defensa es muy bueno en Dungeon World. Amo esta mecánica que promueve el trabajo en equipo y creo un puntero bien defendido es algo glorioso de ver.

### APENDICE D: TEMAS COMUNES EN LA CREACIÓN DE PERSONAJES: {#apendice-d-temas-comunes-en-la-creación-de-personajes}

1.  "Los PG están basados en su puntos de Constitución, no el modificador CON."
2.  "La grilla de atributos estándar está en la línea finita arriba del bloque de atributos. Si, lo sé es difícil de ver."
3.  "Toda la armadura que tienen se agrega a sus puntos de armadura"
4.  “Los Magos de primer nivel tienen tres hechizos en su libro de hechizos. Pueden tener dos de ellos memorizados en cualquier momento. pueden tirar lanzar hechizos todas las veces que quieran, pero unas tiradas bajas en su movimiento de Lanzar Hechizo puede hacer que se olvide del hechizo.
5.  "Comúnmente obtienen PX en las pifias y hacer cosas de sus alineaciones, pero para este tipo de partidas no importa mucho, vamos a subir de nivel solo una vez y hasta la mitad.

## GRACIAS A: {#gracias-a}

Gray Pawn, Morgan Stinson, Ross Cowman, Erich Lichnock, Philip LaRose, Jamie Fristrom, Jay Loomis, Victoria Garcia, and everyone else I’ve ever played with experimented on at Games on Demand.

## OTRAS LECTURAS {#otras-lecturas}

- <span class="underline">Play Unsafe</span> bDE Graham Walmsley es una excepcional guía para el uso de tecnicas de improvisación en juegos de rol de mesa.
- [Cosas a hacer en tu Primer sesión juego de Dungeon World](https://docs.google.com/document/d/1f2MR7NyN41T9QxsYXUhMJCUvXWWX0VYxp1tAtYiOxWs/edit) por Tim Franzke tiene un montón de terribles sugerencias que ayudaran a que seas un fanático de los personajes.

## OTRAS CONVERSACIONES {#otras-conversaciones}

Vean más cosas mías en <http://john.aegard.com>

Si quieren hablar sobre otras cosas, pueden encontrarme en Google Plus, que es donde suelo tener la mayoría de mis conversaciones sobre RPG en estos días. ¡Son bienvenidos sus comentarios e historias de guerra!

## HISTORIAL DE REVISIONES {#historial-de-revisiones}

2014-09-23: Publicación inicial.

2015-08-12: Nuevos vínculos agregados y movimientos de interferencia.
