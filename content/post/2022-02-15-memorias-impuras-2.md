
+++
title = "Memorias Impuras #2"
lastmod = 2022-02-15T17:40:13-03:00
tags = ["python"]
categories = ["nerdeadas", "memoriasImpuras"]
draft = false
image = "https://c.tenor.com/StxEAW_3sasAAAAS/gintama-anime.gif"
+++
Hoy tuvimos taller de python en [ShipHero](https://shiphero.com/)

Entonces ¿Qué aprendí hoy?
- Que la clase `numbers.Number` es la clase raíz de la jerarquía de números en python y contiene a todos los denmás números -> [Clase Numbers](https://docs.python.org/es/3/library/numbers.html#module-numbers)
- Puedo definir un [docstring](https://docs.python.org/es/3/glossary.html#term-docstring) apelando al método "dunder" [`__doc__`](https://docs.python.org/es/3/library/types.html?highlight=__doc__#types.ModuleType.__doc__) así este código:
<!--more-->
```python
def cuadrado(num):
    """
    Función que calcula el cuadrado de un numero
    """
    return num**2
```
Puede escribirse así también:
```python
def cuadrado2(num):
    return num**2

cuadrado2.__doc__="Función que calcula el cuadrado de un numero"
```
- Por fin pude entender bien los parametros arbitrarios `*args` y `**kwargs` El primero me permite tener llamadas a funciones con argumentos posicionales no explícitos y el segundo con argumentos de palabra clave no explícitos. El mejor ejemplo:
```python
def mi_funcion(a1,*args,**kwargs):
    print('a1=', a1)
    print('args=', args)
    print('kwargs=', kwargs)

mi_funcion(4) #Defino solo el primer parámetro obligatorio (a1)
mi_funcion('valor', 1, 2) #Defino "a1" obligatorio y 2 no argumentos no obligatorios arbitrarios que pasan como tuplas
mi_funcion('2', "esto va a una tupla", color='azul', detallado=True) # En este incluyo 2 argumentos de palabra clave que pasan en un diccionario
```
{{< figure src="/images/arg-kwargs.png" >}}

- La importancia de escribir buenas descripciones en los tickets, issues o Pull Request se resumen en esta frase "Cuando terminés leelo y ponete en el lugar de alguien que sepa tecnicamente las cosas pero no tenga idea del ticket a ver si lo entenderías, si la respuesta es si. Entonces estamos"


![](https://c.tenor.com/StxEAW_3sasAAAAS/gintama-anime.gif)
[Gintama está acá](https://tenor.com/view/gintama-anime-number-one-gif-7885634)
