+++
title = "Memorias Impuras #3"
lastmod = 2022-02-17T08:50:26-03:00
tags = ["python", "git", "docker"]
categories = ["memoriasImpuras", "nerdeadas"]
draft = false
image = "https://c.tenor.com/mggHz0aqF7QAAAAM/children-of-the-whales-kujira-no-kora-wa-sajou-ni-utau.gif"
+++

Amo Docker y git. Esto se va a repetir en este post.

La imagen de referencia hoy está algo críptica :P

¿Qué aprendí hoy?

<!--more-->

{{< figure src="/images/children-of-the-whales-kujira-no-kora-wa-sajou-ni-utau.gif" >}}

-   MenCanta [Docker](<https://www.docker.com/>) hoy bajé este blog a mi nuevo Sistema Operativo para editarlo y agregar cosas. No quería instalar [hugo](<https://gohugo.io>) (el gestor del sitio) y las dependencias para ver como quedaban las cosas de forma local. Así que con docker y docker-compose instalado agregué un archivo al blog. Corrí `docker-compose up -d` y ¡listo! Blog corriendo en mi máquina y sin comprometer el Sistema Operativo -&gt; [acá la info](<https://hub.docker.com/r/yanqd0/hugo/>)
-   Aprendí que puedo volver atrás los cambios que no están **estageados** en un repositorio local con `git checkout .` -&gt; este hilo de [StackOverflow lo explica bien](<https://stackoverflow.com/questions/14460595/git-checkout-with-dot>)
-   Estoy aprendiendo a usar CloudWatch un servicio de AWS para el registro de logs. Cuenta con una funcionalidad (Insight) que me permite hacer querys a los logs, y cuando estos se cuentan de millones viene muy bien. La sintaxis es sencilla, pero no se si está muy bien documentada. Pude hacer bien los `filter` pero los `parser` no salieron como esperaba. Me falta aprender.
-   =\__mro\__: Method Resolution Order. Método _dunder_ que no pude usar con el módulo logging, creo que porque usaba una versión de python más nueva que la del [documento que estaba leyendo](<https://realpython.com/python-logging-source-code/>).
