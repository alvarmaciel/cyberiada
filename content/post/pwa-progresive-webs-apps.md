+++
title = "PWA AKA Progresive Webs Apps"
lastmod = 2019-03-26T12:03:12-03:00
tags = ["siglas"]
categories = ["nerdeadas"]
draft = false
image = "https://media.giphy.com/media/ahwb3wJoidsYg/giphy-downsized.gif"
+++

Una de las cosas que necesito es agiornarme. Siglas nuevas, conocimentos nuevos. A veces pienso que esto de saber tecnología es solo mantenerse a tanto de su nomenclatura. Para eso nada mejor de [MDN: recursos para desarrolladores creados por desarrolladores](https://developer.mozilla.org/es/)

**[Progressive Web Apps (PWA)](https://developer.mozilla.org/es/docs/Web/Guide)**
    Las aplicaciones web progresivas usan APIs Web modernas junto con mejoras progresivas estratégicas para crear aplicaciones multi-plataforma. Estas aplicaciones funcionan donde sea y proporcionan muchas características que brindan la misma experiencia de usuario que una aplicación nativa. Este conjunto de documentos y guías te indican sobre todo lo que debes saber de las PWAs.
