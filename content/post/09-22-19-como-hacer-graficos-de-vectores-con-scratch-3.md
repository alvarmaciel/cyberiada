+++
title = "Cómo hacer gráficos de vectores con Scratch 3"
lastmod = 2019-10-01T18:40:20-03:00
tags = ["scratch", "vectores"]
categories = ["traducciones"]
draft = false
image = "https://opensource.com/sites/default/files/styles/image-full-size/public/lead-images/python2-header.png?itok=tEvOVo4A"
subtitle = "Crear nuevos objetos para tu videojuego con la aplicación de dibujo de vectores de Scratch"
+++

Traducción del artículo [How to draw vector graphics with Scratch 3](https://opensource.com/article/19/9/drawing-vectors-scratch-3?sc%5Fcid=70160000001273HAAQ) de [Jess Weichler](https://twitter.com/jlweich) publicado en <https://opensource.com>

<!--more-->

{{< figure src="/images/python2-header.png" caption="Figure 1: Crédito de la imágen: OpenGameArt.org" >}}

Scratch es un popular lenguaje de programación visual que se usa para crear juegos y animaciones. También cuenta con una herramienta de dibujo vectorial que cualquiera puede usar para crear elemntos artísticos únicos para los juegos.

[Scratch 1.0](https://scratch.mit.edu/scratch%5F1.4) fue escrito en [Smaltalk](https://es.wikipedia.org/wiki/Smalltalk), un lenguaje de programación extremadamente hackeable que permite a los usuarios poder mirar el detras de la escena de los programas. Fue popular a traves de distintas plataformas y [forkeado](http://aprendegit.com/fork-de-repositorios-para-que-sirve/) por la [Fundación Raspberry PI](https://www.raspberrypi.org/) para extender su soporte.

[Scratch 3.0](https://scratch.mit.edu/discuss/topic/326861/) fue reescrito desde cero, Está basado en HTML5 y [JavaScript](https://opensource.com/article/19/7/command-line-heroes-javascript), lo que implica que corre perfectamente tanto en tablets y celulares como en laptops o computadoras de escritorio.


## ¿Qué son los vectores? {#qué-son-los-vectores}

El dibujo de vectores es distinto al dibujo con aplicaciones comunes de dibujo. Los dibujos vectoriales se ven definidos, sin importar cuanto zoom hagamos hacia dentro o afuera. No se pixelan. Los vectores crean producciones suaves y acabadas en cualquier tamaño.

En Scratch, los personajes en un juego se llaman **objetos**. Scratch cuenta con una librería de objetos precreados. Los objetos se pueden usar en los proyectos, pero también podemos dibujar nuestros propios objetos usando el programa de dibujo interno o el programa de dibujo de vectores propios de scratch.


## Como comer un elefante {#como-comer-un-elefante}

No necesitmos habilidades de dibujo para dibujar con vectores. En lugar de dibujar un objeto de una sola pasada, lo separamos en formas individuales. Usaremos círculos, óvalos, triángulos y rectángulos. Nos puede ayudar usar fotos o modelos vivos del objeto que queremos dibujar.

Veremos todos los elementos del dibujo de objetos con vectores explicando como dibujar una manzana, por lo cual pueden aplicar este método a cualquier objeto que quieran crear.


## Herramientas de dibujo {#herramientas-de-dibujo}

La caja de herramientas de dibujos vectoriales de Scratch es donde vamos a encontrar todos los elementos que necesitamos para dibujar los objetos:

{{< figure src="/images/scratchtools_v2.png" >}}

Este es el vocabulario asociado al dibujo de vectores:

-   **Canvas o Lienzo**: es donde dibujaremos, el tableros gris y blanco es transparente.
-   **Nodo**: Un punto sobre el trazo de un objeto que determina la forma del objeto.
-   **Objetos**: Un círculo, cuadrado, o línea sobre el lienzo.
-   **Herramienta de selección**: Herramienta para agarrar, modificar tamaño y rotar objetos.
-   **Herramienta de nodos**: Con esta herramienta se pueden mover, agregar y seleccionar nodos.


## Primeros pasos {#primeros-pasos}

Para empezar a dibujar, abran un navegador web y vayan a <https://scratch.mit.edu>. Si están usando la [aplicación de escritorio](https://scratch.mit.edu/download) de scratch, ábranla.

Para abrir un nuevo proyecto, seleccionen **crear** del menú superior. Para lanzar la aplicación de dibujo de vectores, pasen el mouse por el ícono del gato azúl, luego seleccionen el ícono pintar. Esto les creará un nuevo lienzo para el objeto a crear.

{{< figure src="/images/scratchPincel.png" >}}

Hay dos formas de crear un objeto de diseño propio:

-   Crear un objeto completamente nuevo, usando y combinando cualquiera de las herramientas de dibujo de la caja de herramienteas.
-   Cambiar el aspecto de un objeto existente, clickeando en el ícono del objeto para activarlo, y luego yendo a la pestaña _disfraces_ y hacen los cambios que quieran en él.

Unas palabras de advertencia: Si hacen click en el botón **Convertir a mapa de bits**, abajo de la pantalla de dibujo, la ilustración se convertirá en una imagen pixelada tipo bitmap y no podrán volverla a vector.


## Dibujar la forma de una manzana {#dibujar-la-forma-de-una-manzana}

1.  Seleccionen un lienzo de objeto nuevo, luego selecciones la herramienta **círculo**. Creen un círculo haciendo click en el lienzo vacío y arrastrando el mouse. Presionando la tecla Shift mientras hacen esto crearan un círculo perfecto.
2.  Para cambiar el color del círculo, usen la herramienta de **selección**, hagan click en el círculo para seleccionarlo, y click en el menú desplegable debajo de **relleno**. Esto nos da opciones para cambiar el color, la saturación y el brillo de la forma.
    -   Si quieren que la forma sea transparente, sin relleno, seleccionen el cuadrado blanco con la línea diagonal roja para fijar el color a "ninguno".
    -   Si quieren agregar o sacar el borde coloreado alrededor de la forma, seleccionen el menú desplegable que está debajo de **Contorno**.

        {{< figure src="/images/colorForma.png" >}}

3.  click en la herramienta **Nodo**. Hagan click en el centro del objeto para seleccionarlo. verán cuatro nodos separados de forma equitativa alrededor de los bordes del círculo.

    {{< figure src="/images/image3scratch.png" >}}

    Mover cualquiera de los nodos cambiará la forma del círculo. Pueden agregar más nodos haciendo click en el borde del círculo. Si mueven demasiado uno o agregan por accidente un nodo, pueden deshacer este último paso clickeando en el ícono con la flecha hacia atras que se encuentra arriba en la pantalla. También pueden hacer Ctrl+z.
4.  Las manzanas son generalmente más pequeñas debajo que arriba. Hagan click en el lienzo y arrastren hacia donde estarían los nodos laterales. De esta forma seleccionamos los nodos de los costados que se tornaran azules.

    {{< figure src="/images/image4scratch.png" >}}

    Con los nodos seleccionados, presionen la tecla Arriba de su teclado para mover los nodos hacia la parte de arriba del círculo.

    {{< figure src="/images/image5scratch.png" >}}

5.  Agregar dos nodos alrededor del nodo central inferior del círculo, uno en la derecha y otro en la izquierda. Suban el nodo central original un poco hacia arriba para crear una pequeña elevación.

    {{< figure src="/images/image6scratch.png" >}}

6.  ahora agreguen dos nodos en la parte superior del circulo de forma similar a como lo hicieron antes, bajen el nodo central un poco para crear una pequeña depresión.

7.  Continúen ajustando y agregando nodos hasta que estén contentos con la forma de su manzana.


## Agregar el cabo {#agregar-el-cabo}

1.  Seleccionen la herramienta **Rectángulo**. Creen un rectángulo largo y finito donde quieren que esté el cabo de la manzana.
2.  Usen la herramienta **Nodo** para darle una forma similar al cabo de una manzana. Cambien el relleno al color que quieran.
3.  Seleccionen el cabo con la herramienta de **selección**. Para mover el cabo detrás de la manzana, hagan click en el botón **Atrás** que está sobre el lienzo.

    {{< figure src="/images/image7scratch.png" >}}


## Agregar el brillo {#agregar-el-brillo}

1.  Seleccionen la herramienta **Línea**. Dibujen un triángulo en la manzana conectando cada nueva línea con el final de la anterior. Esto convierte las líneas en una forma completa.
2.  Seleccionen el triángulo con la herramienta de **Selección** y cambienlo a un color más brillante, como el blanco, usando la herramienta **Relleno**.
3.  Agreguen y ajusten los nodos con la herraienta **Nodos** para darle forma al brillo. Si los nodos están muy afilados y puntiagudos, pueden cambiarlos a curvas. Seleccionen los nodos que quieren curvos, luego hagan click en el botón **Curvas**. - Tip: para seleccionar más de un nodo a la vez, seleccionen los nodos con la tecla Shift apretada.

    {{< figure src="/images/image8scratch.png" >}}

Y así es como dibujan una manzana que se puede escalar infinitamente.

Si quieren usar la imagen fuera de scratch, hagan click derecho en la miniatura del ícono y seleccionen exportar. Esto hara que puedan bajarse un archivo .svg.

{{< figure src="/images/image9scratch.png" >}}

Para usar la imagen en sus proyectos de Scratch, vuelvan a la pestaña de código. El nuevo objeto aparecerá en la esquina derecha con el resto de sus objetos. Usenlos en sus proyectos de Scratch, compartanlos con otros usuarios en la Web de Scratch, pero sobre todas las cosas -dibujen más cosas copadas con vectores.
