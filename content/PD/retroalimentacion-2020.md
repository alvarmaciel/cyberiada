+++
title = "Retroalimentacion 2020"
lastmod = 2020-12-19T17:06:20-03:00
draft = false
+++

## Análisis de los resultados de las encuestas a DR de Ceibal {#análisis-de-los-resultados-de-las-encuestas-a-dr-de-ceibal}

{{< figure src="/pedagogico/proyectos.png" >}}

- [La Máquina De Dibujar](/pedagogico/lmdd/RetroLaMaquinaDeDibujar.html)
- [ETPA](/pedagogico/etpa/RetroETPA.html)
- [Micro:Gym](/pedagogico/microGym/RetroMicroGym.html)
