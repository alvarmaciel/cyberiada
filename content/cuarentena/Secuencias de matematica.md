+++
title = "Matemática"
lastmod = 2020-04-13T11:13:08-03:00
draft = false
+++

En esta página se irán actualizando las secuencias de trabajo matemático

-   [Cómo estamos hoy](https://student.desmos.com?prepopulateCode=mczgee)
-   [Numeración 1](https://student.desmos.com?prepopulateCode=a8wp4y)
-   [Leer y escribir números grandes](https://student.desmos.com?prepopulateCode=jana8u)
-   [Los números escritos y el valor de sus cifras: los agrupapientos de 10, 100 y 1000](https://teacher.desmos.com/activitybuilder/custom/5e946626ec8f710cac657687)
