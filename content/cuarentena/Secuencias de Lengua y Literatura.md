+++
title = "Lengua y Literatura"
lastmod = 2020-04-02T10:40:58-03:00
draft = false
+++

En esta página se ierán actualizando las secuencias de trabajo de Lengua y Literatura

-   [Los sueños del sapo](https://teacher.desmos.com/activitybuilder/custom/5e7df0588d52ff0cce2c3753)
-   [Limerick 1](https://teacher.desmos.com/activitybuilder/custom/5e8212961f50746feaa5c098)
-   [Cuento breve | Algo muy grave va a pasar en este pueblo](https://teacher.desmos.com/activitybuilder/custom/5e85df44a0f13f0d2723f16a)
-   [Limerick | tortuga con plancha](https://teacher.desmos.com/activitybuilder/custom/5e85e64922ef620c7ff16b01)
