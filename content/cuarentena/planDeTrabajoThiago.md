+++
title = "Plan De Trabajo Secundaria"
lastmod = 2020-07-14T12:49:05-03:00
draft = false
+++

## El plan de trabajo {#el-plan-de-trabajo}

Se propone un plan organizado en realción al horario de clases. Cada día una serie de actividades cortas sonre cada materia.

{{< figure src="/ox-hugo/horarioThiago.jpeg" caption="Figure 1: Horario 2020" >}}

Cada día T. tendrá una serie de actividades relacionadas con la organización horara de su escuela. Estas actividades serán adaptaciones que el Profesor Integrador (PI) hará a partir de las secuencias que los profesores están enviando a través del [campus a los estudiantes](https://ipet424.edu.ar/).

Dado que no se pudo profundizar en la integración, y por motivos que la cuarentena y al suspensión de clases demandan. En este período especial el PI adaptará los contenidos, publicando dichas adaptaciones en el contenido de esta página.

Cada día T. deberá revisar una tabla con sus actividades y marcar en una casilla cuando la actividad está terminada.

Esta tabla recolecta las actividades por día.

- <https://docs.google.com/spreadsheets/d/1c6MV2zgasxRQVK4aFObBESsGLn7xXst5pTjW1bOeebM/edit?usp=sharing>

## Áreas contenidos y adaptaciones {#áreas-contenidos-y-adaptaciones}

### Matemática {#matemática}

<!--list-separator-->

- Trabajos enviados vía campus

   <!--list-separator-->

  - [Cuadernillo de actividades 1](/ox-hugo/CUADERNILLO-DE-ACTIVIDADES.pdf)

     <!--list-separator-->

    - Contenido

      - Numeración
      - Operaciones con números naturales
      - Situaciones ṕroblemáticas
      - Geometría: Rectas, ángulos, polígonos y medida

     <!--list-separator-->

    - Adaptaciones

      - [x] [Numeración](https://teacher.desmos.com/activitybuilder/custom/5ea0592419dff40c807ff064)
      - [x] [Operaciones y problemas. Adaptación de contenido](https://teacher.desmos.com/activitybuilder/custom/5ec29f8f4a9708790db6b24f)
      - [[ ] Problemas del campo multiplicativo. Adaptación de contenido](https://teacher.desmos.com/activitybuilder/custom/5ec2a3902b56500c7ee8d725)

   <!--list-separator-->

  - [Cuadernillo de actividades 2](/ox-hugo/actividades-2dematematica-para-la-cuarentena.pdf)

   <!--list-separator-->

  - [Ejercicios combinados](/ox-hugo/ejercicios-combinados.docx)

### Lengua {#lengua}

<!--list-separator-->

- Trabajos enviados vía campus

   <!--list-separator-->

  - Algunos cuentos para compartir

     <!--list-separator-->

    - Adaptaciones

      - [x] [La extraña Pajarería](https://teacher.desmos.com/activitybuilder/custom/5eb2d2391dc0570ab82babde)
      - [x] [El despertar de pesadillo](https://teacher.desmos.com/activitybuilder/custom/5eb2c9bd3e76d70ca2779009)

   <!--list-separator-->

  - [Alfabeto](/ox-hugo/NUEVO-TEMA-EL-ALFABETO.pdf)

     <!--list-separator-->

    - Adaptación

      - [x] [El alfabeto, adaptación de soporte](https://teacher.desmos.com/activitybuilder/custom/5ed8f712aa630123e8976843)

   <!--list-separator-->

  - Repasamos

### Ciencias naturales - Biología {#ciencias-naturales-biología}

<!--list-separator-->

- Trabajos enviados vía campus

   <!--list-separator-->

  - [Caracterísitcas de los seres vivos](/ox-hugo/1-ANO-B-Cs.-Nat.-Biologia.docx)

     <!--list-separator-->

    - Adaptación

      - [x] [Carcaterísticas de los seres vivos1](https://teacher.desmos.com/activitybuilder/custom/5eb3fb9cf8c98d0cc6950ec8)

   <!--list-separator-->

  - [Reproducción de las plantas](/ox-hugo/1-ANO-B-Cs.-Nat.-Biologia-3S-1.odt)

    - [x] [Adaptación de soporte (documento editable vía web)](https://docs.google.com/document/d/1quEcFfy7-J6WLoXCIOFjiugo9y1bmswjkJLgqZckHuk/edit?usp=sharing)

### Ciudadanía y Participación {#ciudadanía-y-participación}

<!--list-separator-->

- Trabajos vía campus

   <!--list-separator-->

  - [Ciudadanía y Participación Tarea N° 1](/ox-hugo/ciudadania-1-tarea-N-1.docx)

     <!--list-separator-->

    - Adaptación

      - [x] [Coronavirus](https://teacher.desmos.com/activitybuilder/custom/5ec29a372b56500c7ee8c9f9)

   <!--list-separator-->

  - [Tarea 2 OMS](/ox-hugo/Tarea-N-2-Ciudadania.odt)

    - [x] [Adpatación de soporte (documento editable vía web)](https://docs.google.com/document/d/1xGvNIjbrFzcjLhz84eqc8yGM7H5ja58TjKdQtTRiitk/edit?usp=sharing)

### Ciencias Sociales - Geografía {#ciencias-sociales-geografía}

<!--list-separator-->

- Trabajos enviados vía campus

   <!--list-separator-->

  - [Coronavirus](/ox-hugo/CORONAVIRUS.pdf)

     <!--list-separator-->

    - Adaptación

      - [Coronavirus](https://teacher.desmos.com/activitybuilder/custom/5ec2959c30a5460c4b5d7dde)

### Artes visuales {#artes-visuales}

<!--list-separator-->

- Trabajos enviados vía campus

   <!--list-separator-->

  - [Zentangle Art](/ox-hugo/Artes-Visuales-1er-ano-ipet-424.docx)

    Sin adaptaciones

### Lengua Extranjera {#lengua-extranjera}

### Ciencias naturales - Física {#ciencias-naturales-física}

### Dibujo Técnico {#dibujo-técnico}

### Educación Física {#educación-física}

### Electricidad básica {#electricidad-básica}

### Educación tecnológica {#educación-tecnológica}

### Tecnologías en madera {#tecnologías-en-madera}

### Taller Laboratorio {#taller-laboratorio}
